﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VeterinaryClinic.Models;

namespace VeterinaryClinic.Views.Login
{
    public partial class LoginForm : Form
    {
        List<User> users;

        // Prefix for status label
        readonly string statusPre = "Status: ";

        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            // Take Ui thread context for db connection
            SynchronizationContext formContext = SynchronizationContext.Current;

            // Connect to db async
            GetUsersAsync(formContext);
        }

        private async void GetUsersAsync(object context)
        {
            SynchronizationContext formCont = (SynchronizationContext)context;

            await Task.Run(() =>
            {
                try
                {
                    // Get users
                    using (var context = new VeterinaryClinicDbContext())
                    {
                        users = context.Users.Select(u => new User() { Username = u.Username, Password = u.Password }).ToList();
                    }

                    // If there are some users - continue to work, else - show connection error
                    if (users.Count() > 0)
                    {
                        formCont.Send((t) =>
                        {
                            submitButton.Enabled = true;
                            ChangeStatusLabel("Database connected successfully.", Color.Green);
                        }, null
                        );
                    }
                    else
                    {
                        throw new Exception("Connection error.");
                    }
                }
                catch (Exception e)
                {
                    formCont.Send((p) => {
                        ChangeStatusLabel(e.Message, Color.Red);
                    }
                    , null);
                }
            });
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            if (users.Find(u => u.Username == tbUsername.Text && u.Password == tbPassword.Text) != null)
            {
                ChangeStatusLabel("Connected", Color.Green);
                this.Text = "Loading...";
                Thread.Sleep(500);
                this.DialogResult = DialogResult.OK;
                this.Close();
            } else
            {
                ChangeStatusLabel("Username or password incorrect.", Color.Red);
            }
        }

        // Change text and color of status label
        private void ChangeStatusLabel(string text, Color color)
        {
            statusLabel.ForeColor = color;
            statusLabel.Text = statusPre + text;
        }
    }
}
