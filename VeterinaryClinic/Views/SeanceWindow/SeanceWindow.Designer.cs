﻿
namespace VeterinaryClinic.Views.SeanceWindow
{
    partial class SeanceWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.doctorLabel = new System.Windows.Forms.Label();
            this.clientLabel = new System.Windows.Forms.Label();
            this.petLabel = new System.Windows.Forms.Label();
            this.datePickerLabel = new System.Windows.Forms.Label();
            this.statusLabel = new System.Windows.Forms.Label();
            this.doctorCb = new System.Windows.Forms.ComboBox();
            this.clientCb = new System.Windows.Forms.ComboBox();
            this.petCb = new System.Windows.Forms.ComboBox();
            this.procedureGrB = new System.Windows.Forms.GroupBox();
            this.dateOfSeance = new System.Windows.Forms.DateTimePicker();
            this.submitBtn = new System.Windows.Forms.Button();
            this.closeBtn = new System.Windows.Forms.Button();
            this.IdLabel = new System.Windows.Forms.Label();
            this.idValueLabel = new System.Windows.Forms.Label();
            this.reportBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // doctorLabel
            // 
            this.doctorLabel.AutoSize = true;
            this.doctorLabel.Location = new System.Drawing.Point(39, 89);
            this.doctorLabel.Name = "doctorLabel";
            this.doctorLabel.Size = new System.Drawing.Size(81, 15);
            this.doctorLabel.TabIndex = 0;
            this.doctorLabel.Text = "Chose doctor:";
            // 
            // clientLabel
            // 
            this.clientLabel.AutoSize = true;
            this.clientLabel.Location = new System.Drawing.Point(39, 140);
            this.clientLabel.Name = "clientLabel";
            this.clientLabel.Size = new System.Drawing.Size(75, 15);
            this.clientLabel.TabIndex = 1;
            this.clientLabel.Text = "Chose client:";
            // 
            // petLabel
            // 
            this.petLabel.AutoSize = true;
            this.petLabel.Location = new System.Drawing.Point(39, 189);
            this.petLabel.Name = "petLabel";
            this.petLabel.Size = new System.Drawing.Size(63, 15);
            this.petLabel.TabIndex = 2;
            this.petLabel.Text = "Chose pet:";
            // 
            // datePickerLabel
            // 
            this.datePickerLabel.AutoSize = true;
            this.datePickerLabel.Location = new System.Drawing.Point(39, 241);
            this.datePickerLabel.Name = "datePickerLabel";
            this.datePickerLabel.Size = new System.Drawing.Size(69, 15);
            this.datePickerLabel.TabIndex = 3;
            this.datePickerLabel.Text = "Chose date:";
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(39, 322);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(42, 15);
            this.statusLabel.TabIndex = 6;
            this.statusLabel.Text = "Status:";
            // 
            // doctorCb
            // 
            this.doctorCb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.doctorCb.FormattingEnabled = true;
            this.doctorCb.Location = new System.Drawing.Point(39, 108);
            this.doctorCb.Name = "doctorCb";
            this.doctorCb.Size = new System.Drawing.Size(208, 23);
            this.doctorCb.TabIndex = 7;
            // 
            // clientCb
            // 
            this.clientCb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.clientCb.FormattingEnabled = true;
            this.clientCb.Location = new System.Drawing.Point(39, 159);
            this.clientCb.Name = "clientCb";
            this.clientCb.Size = new System.Drawing.Size(208, 23);
            this.clientCb.TabIndex = 8;
            // 
            // petCb
            // 
            this.petCb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.petCb.FormattingEnabled = true;
            this.petCb.Location = new System.Drawing.Point(39, 207);
            this.petCb.Name = "petCb";
            this.petCb.Size = new System.Drawing.Size(208, 23);
            this.petCb.TabIndex = 9;
            // 
            // procedureGrB
            // 
            this.procedureGrB.Location = new System.Drawing.Point(270, 25);
            this.procedureGrB.Name = "procedureGrB";
            this.procedureGrB.Size = new System.Drawing.Size(244, 282);
            this.procedureGrB.TabIndex = 10;
            this.procedureGrB.TabStop = false;
            this.procedureGrB.Text = "Chose procedures:";
            // 
            // dateOfSeance
            // 
            this.dateOfSeance.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateOfSeance.Location = new System.Drawing.Point(39, 259);
            this.dateOfSeance.Name = "dateOfSeance";
            this.dateOfSeance.Size = new System.Drawing.Size(208, 23);
            this.dateOfSeance.TabIndex = 11;
            // 
            // submitBtn
            // 
            this.submitBtn.Location = new System.Drawing.Point(39, 403);
            this.submitBtn.Name = "submitBtn";
            this.submitBtn.Size = new System.Drawing.Size(75, 39);
            this.submitBtn.TabIndex = 13;
            this.submitBtn.Text = "Submit";
            this.submitBtn.UseVisualStyleBackColor = true;
            this.submitBtn.Click += new System.EventHandler(this.submitBtn_Click);
            // 
            // closeBtn
            // 
            this.closeBtn.Location = new System.Drawing.Point(440, 403);
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(74, 39);
            this.closeBtn.TabIndex = 14;
            this.closeBtn.Text = "Close";
            this.closeBtn.UseVisualStyleBackColor = true;
            this.closeBtn.Click += new System.EventHandler(this.closeBtn_Click);
            // 
            // IdLabel
            // 
            this.IdLabel.AutoSize = true;
            this.IdLabel.Location = new System.Drawing.Point(39, 40);
            this.IdLabel.Name = "IdLabel";
            this.IdLabel.Size = new System.Drawing.Size(60, 15);
            this.IdLabel.TabIndex = 15;
            this.IdLabel.Text = "Record №";
            // 
            // idValueLabel
            // 
            this.idValueLabel.AutoSize = true;
            this.idValueLabel.Location = new System.Drawing.Point(100, 40);
            this.idValueLabel.Name = "idValueLabel";
            this.idValueLabel.Size = new System.Drawing.Size(14, 15);
            this.idValueLabel.TabIndex = 16;
            this.idValueLabel.Text = "#";
            // 
            // reportBtn
            // 
            this.reportBtn.Enabled = false;
            this.reportBtn.Location = new System.Drawing.Point(120, 403);
            this.reportBtn.Name = "reportBtn";
            this.reportBtn.Size = new System.Drawing.Size(80, 39);
            this.reportBtn.TabIndex = 17;
            this.reportBtn.Text = "To PDF";
            this.reportBtn.UseVisualStyleBackColor = true;
            this.reportBtn.Click += new System.EventHandler(this.reportBtn_Click);
            // 
            // SeanceWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 466);
            this.Controls.Add(this.reportBtn);
            this.Controls.Add(this.idValueLabel);
            this.Controls.Add(this.IdLabel);
            this.Controls.Add(this.closeBtn);
            this.Controls.Add(this.submitBtn);
            this.Controls.Add(this.dateOfSeance);
            this.Controls.Add(this.procedureGrB);
            this.Controls.Add(this.petCb);
            this.Controls.Add(this.clientCb);
            this.Controls.Add(this.doctorCb);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.datePickerLabel);
            this.Controls.Add(this.petLabel);
            this.Controls.Add(this.clientLabel);
            this.Controls.Add(this.doctorLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SeanceWindow";
            this.Text = "Seance:";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label doctorLabel;
        private System.Windows.Forms.Label clientLabel;
        private System.Windows.Forms.Label petLabel;
        private System.Windows.Forms.Label datePickerLabel;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.ComboBox doctorCb;
        private System.Windows.Forms.ComboBox clientCb;
        private System.Windows.Forms.ComboBox petCb;
        private System.Windows.Forms.GroupBox procedureGrB;
        private System.Windows.Forms.DateTimePicker dateOfSeance;
        private System.Windows.Forms.Button submitBtn;
        private System.Windows.Forms.Button closeBtn;
        private System.Windows.Forms.Label IdLabel;
        private System.Windows.Forms.Label idValueLabel;
        private System.Windows.Forms.Button reportBtn;
    }
}