﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VeterinaryClinic.Components;
using VeterinaryClinic.Models;
using System.IO;
using iText.Layout;
using iText.Kernel.Pdf;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Kernel.Colors;
using Microsoft.EntityFrameworkCore;

namespace VeterinaryClinic.Views.SeanceWindow
{
    public partial class SeanceWindow : Form
    {
        // After SeanceWindow closing return selected date
        public DateTime ResultDate { get; set; }

        // -1 - new seance - CREATE
        // >0 - old seance - MODIFY
        // isChangable - { true: add/modify, false: info}
        public SeanceWindow(int seanceId, bool isChangable)
        {
            InitializeComponent();
            dateOfSeance.CustomFormat = "dd MMMM yyyy ,  HH:mm";

            using (var context = new VeterinaryClinicDbContext())
            {
                Seance seance = null;
                int itemLocationStep = 30,
                    items = 1;

                if (seanceId > 0)
                    seance = context.Seances.First(s => s.Id == seanceId);

                foreach (var d in context.Doctors.Select(d => new { d.Id, d.PersonName }).ToList())
                    doctorCb.Items.Add(new SeanceComboItem() { EntityId = d.Id, EntityName = d.PersonName });

                foreach (var c in context.Clients.Select(d => new { d.Id, d.PersonName }).ToList())
                    clientCb.Items.Add(new SeanceComboItem() { EntityId = c.Id, EntityName = c.PersonName });
                // Change pets in combobox with changing client
                clientCb.SelectedIndexChanged += UploadPets;

                foreach (var p in context.VetProcedures.Select(p => new { p.Id, p.ProcedureName, p.Duration }).ToList())
                {
                    ProcedureCheckBox proc = new ProcedureCheckBox();
                    proc.Text = p.ProcedureName;
                    proc.ProcedureId = p.Id;
                    proc.Duration = p.Duration;
                    proc.Location = new Point(10, itemLocationStep * items++);

                    procedureGrB.Controls.Add(proc);
                }

                if (isChangable == false)
                {
                    doctorCb.Enabled = false;
                    clientCb.Enabled = false;
                    petCb.Enabled = false;
                    dateOfSeance.Enabled = false;
                    submitBtn.Enabled = false;
                    statusLabel.Visible = false;
                    reportBtn.Enabled = true;

                    foreach (ProcedureCheckBox proc in procedureGrB.Controls)
                    {
                        proc.Enabled = false;
                    }
                }

                // Input all old data to textbox, combobox... etc
                if (seanceId > 0)
                {
                    ChoseValues(seance);
                }
            }
        }

        private void submitBtn_Click(object sender, EventArgs e)
        {
            var errorList = ValidateData();

            if (errorList.Count() > 0)
            {
                statusLabel.Text = "Status:  ";
                statusLabel.ForeColor = System.Drawing.Color.Red;

                foreach (string s in errorList)
                {
                    if (errorList.IndexOf(s) == errorList.Count() - 1)
                        statusLabel.Text += s + ".";
                    else
                        statusLabel.Text += s + ", ";
                }

                return;
            }

            try 
            { 
                using (var context = new VeterinaryClinicDbContext())
                {
                    if (idValueLabel.Text == "#")
                    {
                        Seance seance = new Seance()
                        {
                            DoctorId = ((SeanceComboItem)doctorCb.SelectedItem).EntityId,
                            PetId = ((SeanceComboItem)petCb.SelectedItem).EntityId,
                            SeanceDate = dateOfSeance.Value,
                        };

                        foreach (ProcedureCheckBox item in procedureGrB.Controls)
                        {
                            if (item.Checked == true)
                            {
                                seance.VetProcedures.Add(context.VetProcedures.First(p => p.Id == item.ProcedureId));
                            }
                        }

                        context.Seances.Add(seance);
                    }
                    else
                    {
                        var curSeance = context.Seances.First(s => s.Id == Convert.ToInt32(idValueLabel.Text));
                        curSeance.PetId = ((SeanceComboItem)petCb.SelectedItem).EntityId;
                        curSeance.DoctorId = ((SeanceComboItem)doctorCb.SelectedItem).EntityId;
                        curSeance.SeanceDate = dateOfSeance.Value;

                        curSeance.VetProcedures.Clear();
                        foreach (ProcedureCheckBox item in procedureGrB.Controls)
                        {
                            if (item.Checked == true)
                            {
                                curSeance.VetProcedures.Add(context.VetProcedures.First(p => p.Id == item.ProcedureId));
                            }
                        }

                        context.Seances.Update(curSeance);
                    }

                    context.SaveChanges();
                }

                statusLabel.Text = "Status:  Success";
                statusLabel.ForeColor = System.Drawing.Color.Green;
                ResultDate = dateOfSeance.Value;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                statusLabel.Text = "Status:  " + ex.Message + " ;";
                statusLabel.ForeColor = System.Drawing.Color.Red;
            }
        }

        private void UploadPets(object sender, EventArgs e) 
        {
            using (var context = new VeterinaryClinicDbContext())
            {
                var pets = context.Pets
                    .Where(p => p.ClientId == ((SeanceComboItem)clientCb.SelectedItem).EntityId)
                    .Select(p => new { p.Id, p.PetName }).ToList();

                petCb.Items.Clear();
                foreach (var pet in pets)
                {
                    petCb.Items.Add(new SeanceComboItem() { EntityId = pet.Id, EntityName = pet.PetName });
                }

                if (pets.Count > 0)
                {
                    petCb.SelectedIndex = 0;
                }
            }
        }

        // Select values in controls (textbox, combobox... etc) at modifying data
        private void ChoseValues(Seance seance)
        {
            idValueLabel.Text = seance.Id.ToString();
            dateOfSeance.Value = seance.SeanceDate;

            foreach (SeanceComboItem item in doctorCb.Items)
            {
                if (item.EntityId == seance.DoctorId)
                {
                    doctorCb.SelectedItem = item;
                    break;
                }
            }

            foreach (SeanceComboItem item in clientCb.Items)
            {
                if (item.EntityId == seance.Pet.ClientId)
                {
                    clientCb.SelectedItem = item;
                    break;
                }
            }

            foreach (SeanceComboItem item in petCb.Items)
            {
                if (item.EntityId == seance.PetId)
                {
                    petCb.SelectedItem = item;
                    break;
                }
            }

            foreach (ProcedureCheckBox item in procedureGrB.Controls)
            {
                foreach (var proc in seance.VetProcedures.Select(p => new { p.Id, p.ProcedureName}).ToList())
                {
                    if (proc.Id == item.ProcedureId)
                    {
                        item.Checked = true;
                    }
                }
            }
        }

        private List<string> ValidateData()
        {
            List<string> errorList = new List<string>();

            // Check, if selected date is in past
            int isPast = DateTime.Compare(DateTime.Now, dateOfSeance.Value);
            decimal proceduresDuration = 0;

            foreach (ProcedureCheckBox item in procedureGrB.Controls)
            {
                if (item.Checked == true)
                {
                    proceduresDuration += item.Duration;
                }
            }

            // Fields validation
            if (doctorCb.SelectedItem == null)
            {
                errorList.Add("select doctor");
            }

            if (clientCb.SelectedItem == null)
            {
                errorList.Add("select client");
            }

            if (petCb.SelectedItem == null)
            {
                errorList.Add("select pet");
            }

            if (isPast > 0)
            {
                errorList.Add("data is incorrect");
            }

            if (proceduresDuration == 0)
            {
                errorList.Add("select procedure(s)");
            }

            // If some fields selected not correct return errors
            if (errorList.Count > 0)
            {
                return errorList;
            }

            // Logic validation
            using (var context = new VeterinaryClinicDbContext())
            {
                // Get doctors and pets schedules for selected date
                var docsSchedule = context.Seances
                    .Where(s => s.SeanceDate.Date == dateOfSeance.Value.Date
                        && s.DoctorId == ((SeanceComboItem)doctorCb.SelectedItem).EntityId)
                    .ToList();
                var petsSchedule = context.Seances
                    .Where(s => s.SeanceDate.Date == dateOfSeance.Value.Date
                        && s.PetId == ((SeanceComboItem)petCb.SelectedItem).EntityId)
                    .ToList();

                DateTime startTime = dateOfSeance.Value,
                         endTime = dateOfSeance.Value.AddMinutes(Convert.ToDouble(proceduresDuration));

                double totalProceduresTime = 0;

                // Check, if doctor has another procedures at selected time
                foreach (Seance s in docsSchedule)
                {
                    totalProceduresTime = Convert.ToDouble(s.VetProcedures.Sum(p => p.Duration));

                    if ((startTime >= s.SeanceDate &&
                        startTime <= s.SeanceDate.AddMinutes(totalProceduresTime)) 
                        ||
                        (endTime >= s.SeanceDate &&
                        endTime <= s.SeanceDate.AddMinutes(totalProceduresTime)))
                    {
                        errorList.Add("doctor is busy");
                        break;
                    }
                }

                // Check, if pet has another procedures at selected time
                foreach (Seance s in petsSchedule)
                {
                    totalProceduresTime = Convert.ToDouble(s.VetProcedures.Sum(p => p.Duration));

                    if ((startTime > s.SeanceDate &&
                        startTime < s.SeanceDate.AddMinutes(totalProceduresTime))
                        ||
                        (endTime > s.SeanceDate &&
                        endTime < s.SeanceDate.AddMinutes(totalProceduresTime)))
                    {
                        errorList.Add("pet is busy");
                        break;
                    }
                }
            }

            return errorList;
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void reportBtn_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                BuildReportAsync(dialog.SelectedPath);
            }
        }

        // Build seance report
        private async void BuildReportAsync(string path)
        {
            try 
            { 
                await Task.Run(() => 
                {
                    using (var context = new VeterinaryClinicDbContext())
                    {
                        Seance seance = context.Seances
                            .Include(s => s.VetProcedures)
                            .Include(s => s.Pet)
                            .ThenInclude(p => p.Client)
                            .First(s => s.Id == Convert.ToInt32(idValueLabel.Text));

                        string fileName = "Seance_" + seance.Id.ToString() + ".pdf";

                        PdfWriter writer = new PdfWriter(path + "/" + fileName);
                        PdfDocument pdf = new PdfDocument(writer);
                        Document report = new Document(pdf);

                        Paragraph header = new Paragraph("Seance #" + seance.Id)
                            .SetTextAlignment(TextAlignment.CENTER)
                            .SetFontSize(20);
                        report.Add(header);

                        Paragraph subheader = new Paragraph("Payment report")
                           .SetTextAlignment(TextAlignment.CENTER)
                           .SetFontSize(15);
                        report.Add(subheader);

                        // Line separator
                        LineSeparator ls = new LineSeparator(new SolidLine());
                        report.Add(ls);

                        Paragraph docLine = new Paragraph($"Doctor: {seance.Doctor.PersonName}.")
                           .SetTextAlignment(TextAlignment.LEFT)
                           .SetFontSize(12);
                        report.Add(docLine);

                        Paragraph clientLine = new Paragraph($"Client: {seance.Pet.Client.PersonName}.")
                           .SetTextAlignment(TextAlignment.LEFT)
                           .SetFontSize(12);
                        report.Add(clientLine);

                        Paragraph petLine = new Paragraph($"Pet: {seance.Pet.PetName}, type: {seance.Pet.PetType}, family: {seance.Pet.PetFamily}.")
                           .SetTextAlignment(TextAlignment.LEFT)
                           .SetFontSize(12);
                        report.Add(petLine);

                        Paragraph dateOfProc = new Paragraph($"Date of procedures: {seance.SeanceDate}.\n\n\n\n")
                           .SetTextAlignment(TextAlignment.LEFT)
                           .SetFontSize(12);
                        report.Add(dateOfProc);

                        // Table
                        Table table = new Table(3, false);
                        Cell cell11 = new Cell(1, 1)
                           .SetBackgroundColor(ColorConstants.GRAY)
                           .SetTextAlignment(TextAlignment.CENTER)
                           .SetWidth(100)
                           .Add(new Paragraph("Procedure"));
                        Cell cell12 = new Cell(1, 1)
                           .SetBackgroundColor(ColorConstants.GRAY)
                           .SetTextAlignment(TextAlignment.CENTER)
                           .SetWidth(100)
                           .Add(new Paragraph("Duration (min)"));
                        Cell cell13 = new Cell(1, 1)
                           .SetBackgroundColor(ColorConstants.GRAY)
                           .SetTextAlignment(TextAlignment.CENTER)
                           .SetWidth(100)
                           .Add(new Paragraph("Price ($)"));
                        table.AddCell(cell11);
                        table.AddCell(cell12);
                        table.AddCell(cell13);

                        foreach (VetProcedure p in seance.VetProcedures)
                        {
                            Cell cell1 = new Cell(1, 1)
                                .SetTextAlignment(TextAlignment.CENTER)
                                .Add(new Paragraph(p.ProcedureName));
                            Cell cell2 = new Cell(1, 1)
                                .SetTextAlignment(TextAlignment.CENTER)
                                .Add(new Paragraph(p.Duration.ToString()));
                            Cell cell3 = new Cell(1, 1)
                                .SetTextAlignment(TextAlignment.CENTER)
                                .Add(new Paragraph(p.Price.ToString()));

                            table.AddCell(cell1);
                            table.AddCell(cell2);
                            table.AddCell(cell3);
                        }

                        Cell subLast1 = new Cell(1, 1)
                           .SetTextAlignment(TextAlignment.CENTER)
                           .SetHeight(20)
                           .Add(new Paragraph(""));
                        Cell subLast2 = new Cell(1, 1)
                           .SetTextAlignment(TextAlignment.CENTER)
                           .Add(new Paragraph(""));
                        Cell subLast3 = new Cell(1, 1)
                           .SetTextAlignment(TextAlignment.CENTER)
                           .Add(new Paragraph(""));
                        table.AddCell(subLast1);
                        table.AddCell(subLast2);
                        table.AddCell(subLast3);

                        Cell Last1 = new Cell(1, 1)
                           .SetBackgroundColor(ColorConstants.GRAY)
                           .SetTextAlignment(TextAlignment.CENTER)
                           .Add(new Paragraph("TOTAL"));
                        Cell Last2 = new Cell(1, 1)
                           .SetTextAlignment(TextAlignment.CENTER)
                           .Add(new Paragraph(seance.VetProcedures.Sum(p => p.Duration).ToString()));
                        Cell Last3 = new Cell(1, 1)
                           .SetTextAlignment(TextAlignment.CENTER)
                           .Add(new Paragraph(seance.VetProcedures.Sum(p => p.Price).ToString()));
                        table.AddCell(Last1);
                        table.AddCell(Last2);
                        table.AddCell(Last3);
                        report.Add(table);

                        Paragraph dateOfReport = new Paragraph($"\n\n\n\n\n\nDate of report: {DateTime.Now}.")
                           .SetTextAlignment(TextAlignment.RIGHT)
                           .SetFontSize(12)
                           .SetVerticalAlignment(VerticalAlignment.MIDDLE);
                        Paragraph signification = new Paragraph($"Clients signification: ________")
                           .SetTextAlignment(TextAlignment.RIGHT)
                           .SetFontSize(12)
                           .SetVerticalAlignment(VerticalAlignment.MIDDLE)
                           ;
                        report.Add(dateOfReport);
                        report.Add(signification);

                        report.Close();
                    }
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
