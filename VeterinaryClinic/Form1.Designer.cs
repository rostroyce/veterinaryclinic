﻿
namespace VeterinaryClinic
{
    partial class MainWindow
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NavigationPanel = new System.Windows.Forms.Panel();
            this.btnAddProcedure = new System.Windows.Forms.Button();
            this.btnPet = new System.Windows.Forms.Button();
            this.btnAddClient = new System.Windows.Forms.Button();
            this.btnAddDoctor = new System.Windows.Forms.Button();
            this.btnSchedule = new System.Windows.Forms.Button();
            this.ContentPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.NavigationPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // NavigationPanel
            // 
            this.NavigationPanel.Controls.Add(this.btnAddProcedure);
            this.NavigationPanel.Controls.Add(this.btnPet);
            this.NavigationPanel.Controls.Add(this.btnAddClient);
            this.NavigationPanel.Controls.Add(this.btnAddDoctor);
            this.NavigationPanel.Controls.Add(this.btnSchedule);
            this.NavigationPanel.Location = new System.Drawing.Point(12, 12);
            this.NavigationPanel.Name = "NavigationPanel";
            this.NavigationPanel.Size = new System.Drawing.Size(209, 484);
            this.NavigationPanel.TabIndex = 0;
            // 
            // btnAddProcedure
            // 
            this.btnAddProcedure.Location = new System.Drawing.Point(15, 232);
            this.btnAddProcedure.Name = "btnAddProcedure";
            this.btnAddProcedure.Size = new System.Drawing.Size(176, 31);
            this.btnAddProcedure.TabIndex = 4;
            this.btnAddProcedure.Text = "Add Procedure Info";
            this.btnAddProcedure.UseVisualStyleBackColor = true;
            this.btnAddProcedure.Click += new System.EventHandler(this.btnAddProcedure_Click);
            // 
            // btnPet
            // 
            this.btnPet.Location = new System.Drawing.Point(15, 194);
            this.btnPet.Name = "btnPet";
            this.btnPet.Size = new System.Drawing.Size(176, 31);
            this.btnPet.TabIndex = 3;
            this.btnPet.Text = "Add Pet Info";
            this.btnPet.UseVisualStyleBackColor = true;
            this.btnPet.Click += new System.EventHandler(this.btnPet_Click);
            // 
            // btnAddClient
            // 
            this.btnAddClient.Location = new System.Drawing.Point(15, 155);
            this.btnAddClient.Name = "btnAddClient";
            this.btnAddClient.Size = new System.Drawing.Size(176, 32);
            this.btnAddClient.TabIndex = 2;
            this.btnAddClient.Text = "Add Client Info";
            this.btnAddClient.UseVisualStyleBackColor = true;
            this.btnAddClient.Click += new System.EventHandler(this.btnAddClient_Click);
            // 
            // btnAddDoctor
            // 
            this.btnAddDoctor.Location = new System.Drawing.Point(15, 119);
            this.btnAddDoctor.Name = "btnAddDoctor";
            this.btnAddDoctor.Size = new System.Drawing.Size(176, 30);
            this.btnAddDoctor.TabIndex = 1;
            this.btnAddDoctor.Text = "Add Doctor Info";
            this.btnAddDoctor.UseVisualStyleBackColor = true;
            this.btnAddDoctor.Click += new System.EventHandler(this.btnAddDoctor_Click);
            // 
            // btnSchedule
            // 
            this.btnSchedule.Location = new System.Drawing.Point(15, 15);
            this.btnSchedule.Name = "btnSchedule";
            this.btnSchedule.Size = new System.Drawing.Size(176, 84);
            this.btnSchedule.TabIndex = 0;
            this.btnSchedule.Text = "Schedule";
            this.btnSchedule.UseVisualStyleBackColor = true;
            this.btnSchedule.Click += new System.EventHandler(this.btnSchedule_Click);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Location = new System.Drawing.Point(228, 12);
            this.ContentPanel.Name = "ContentPanel";
            this.ContentPanel.Size = new System.Drawing.Size(928, 484);
            this.ContentPanel.TabIndex = 1;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1168, 508);
            this.Controls.Add(this.ContentPanel);
            this.Controls.Add(this.NavigationPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Veterinary Clinic";
            this.Load += new System.EventHandler(this.AppClient_Load);
            this.NavigationPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel NavigationPanel;
        private System.Windows.Forms.Button btnAddProcedure;
        private System.Windows.Forms.Button btnPet;
        private System.Windows.Forms.Button btnAddClient;
        private System.Windows.Forms.Button btnAddDoctor;
        private System.Windows.Forms.Button btnSchedule;
        private System.Windows.Forms.FlowLayoutPanel ContentPanel;
    }
}

