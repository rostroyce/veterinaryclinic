﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VeterinaryClinic.Components;
using VeterinaryClinic.Models;


namespace VeterinaryClinic.Helpers
{
    class PanelPets : PanelBase
    {
        public PanelPets(FlowLayoutPanel destinationPanel) : base(destinationPanel)
        {

        }

        public override void BuildPanel()
        {
            IEnumerable<Pet> pets;
            using (var context = new VeterinaryClinicDbContext())
            {
                pets = context.Pets.Include(pet => pet.Client).AsNoTracking().ToList();
            }

            #region input fields
            // Panel with work fields
            Panel fieldsPanel = CreateTextFieldsPanel();

            int locX = (int)(fieldsPanel.Width * 0.1),
                startLocY = (int)(fieldsPanel.Height * 0.1),
                tbWidth = (int)(fieldsPanel.Width * 0.8),
                stepLow = 25,
                stepFull;

            Label idLabel = new Label();
            idLabel.Location = new Point(10, 10);
            idLabel.Text = "Record №";
            idLabel.Width = 60;
            Label idValueLabel = new Label();
            idValueLabel.Location = new Point(70, 10);
            idValueLabel.Text = "#";

            Label nameLabel = new Label();
            nameLabel.Width = tbWidth;
            nameLabel.Text = "Enter pets name:";
            nameLabel.Location = new Point(locX, startLocY);
            TextBox nameTb = new TextBox();
            nameTb.Location = new Point(locX, nameLabel.Location.Y + stepLow);
            nameTb.Width = tbWidth;
            nameTb.MaxLength = 60;

            // Calculate height of label+textBox
            stepFull = nameTb.Location.Y + nameTb.Height - nameLabel.Location.Y + 10;

            Label typeLabel = new Label();
            typeLabel.Location = new Point(locX, startLocY + stepFull);
            typeLabel.Text = "Chose animal type:";
            typeLabel.Width = tbWidth;
            ComboBox typeCb = new ComboBox();
            typeCb.Location = new Point(locX, typeLabel.Location.Y + stepLow);
            typeCb.Width = tbWidth;
            typeCb.DropDownStyle = ComboBoxStyle.DropDownList;

            // Add animal type enum to combobox
            foreach (AnimalTypes nav in Enum.GetValues(typeof(AnimalTypes)))
            {
                typeCb.Items.Add(nav);

                if (typeCb.Items.Count > 0)
                {
                    typeCb.SelectedIndex = 0;
                }
            }

            Label familyLabel = new Label();
            familyLabel.Location = new Point(locX, startLocY + stepFull * 2);
            familyLabel.Text = "Enter pets family:";
            familyLabel.Width = tbWidth;
            TextBox familyTb = new TextBox();
            familyTb.Location = new Point(locX, familyLabel.Location.Y + stepLow);
            familyTb.Width = tbWidth;


            Label weightLabel = new Label();
            weightLabel.Location = new Point(locX, startLocY + stepFull * 3);
            weightLabel.Text = "Enter pets Weight:";
            weightLabel.Width = tbWidth;
            TextBox weightTb = new TextBox();
            weightTb.Location = new Point(locX, weightLabel.Location.Y + stepLow);
            weightTb.Width = tbWidth;

            Label masterLabel = new Label();
            masterLabel.Location = new Point(locX, startLocY + stepFull * 4);
            masterLabel.Text = "Chose master:";
            masterLabel.Width = tbWidth;
            ComboBox masterCb = new ComboBox();
            masterCb.Location = new Point(locX, masterLabel.Location.Y + stepLow);
            masterCb.Width = tbWidth;
            masterCb.DropDownStyle = ComboBoxStyle.DropDownList;

            // Add all clients to combobox
            using (var context = new VeterinaryClinicDbContext())
            {
                foreach (var client in context.Clients.Select(c => new { c.Id, c.PersonName }).OrderBy(c => c.PersonName).ToList())
                {
                    masterCb.Items.Add(new PetMasterComboItem() { Id = client.Id, ClientName = client.PersonName });
                }

                if (masterCb.Items.Count > 0)
                {
                    masterCb.SelectedIndex = 0;
                }
            }
            #endregion

            Label statusLabel = new Label();
            statusLabel.Location = new Point(locX, startLocY + stepFull * 5);
            statusLabel.Text = statusPrefix;
            statusLabel.Width = tbWidth;

            // Panel with all pet info
            Panel allInfo = CreateDataGridPanel();
            DataGridView petsGrid = CreateDataGrid(allInfo.Width, allInfo.Height);
            List<Control> searchControls = AddSearchControls(petsGrid, 0, "Search by name:");

            #region action buttons
            Button submitButton = new Button();
            submitButton.Name = "btnSubmit";
            submitButton.Location = new Point(locX, fieldsPanel.Height - 50);
            submitButton.Width = 60;
            submitButton.Text = "Submit";

            Button resetButton = new Button();
            resetButton.Name = "btnReset";
            resetButton.Location = new Point(locX + 80, fieldsPanel.Height - 50);
            resetButton.Width = 60;
            resetButton.Text = "Reset";

            resetButton.Click += (object sender, EventArgs e) =>
            {
                nameTb.Text = "";
                weightTb.Text = "";
                familyTb.Text = "";
                idValueLabel.Text = "#";

                if (typeCb.Items.Count > 0)
                {
                    typeCb.SelectedIndex = 0;
                }

                if (masterCb.Items.Count > 0)
                {
                    masterCb.SelectedIndex = 0;
                }
            };

            submitButton.Click += (object sender, EventArgs e) =>
            {
                // Simple data validation
                double weight = 0;

                if (nameTb.Text.Length < 2 ||
                    !double.TryParse(weightTb.Text, out weight) ||
                    masterCb.SelectedItem == null ||
                    typeCb.SelectedItem == null
                    )
                {
                    ChangeStatusLabel(statusLabel, "Some data are not correct.", Color.Red);
                    return;
                }

                using (var context = new VeterinaryClinicDbContext())
                {
                    // Check for existing id on panel with data fields
                    // # - not exists(new record) -> create new procedure and add to table
                    // <number> - exists(old record) -> modify procedure data
                    if (idValueLabel.Text == "#")
                    {
                        // Create new procedure and insert into context
                        Pet newPet = new Pet()
                        {
                            PetName = nameTb.Text,
                            PetType = typeCb.SelectedItem.ToString(),
                            PetFamily = familyTb.Text,
                            Weight = weight,
                            ClientId = ((PetMasterComboItem)masterCb.SelectedItem).Id
                        };

                        try
                        {
                            context.Pets.Add(newPet);
                            context.SaveChanges();
                            petsGrid.Rows.Add(
                                newPet.PetName,
                                ((PetMasterComboItem)masterCb.SelectedItem).ClientName,
                                newPet.PetType,
                                newPet.PetFamily,
                                newPet.Weight,
                                newPet.Id,
                                "R", "M");
                            resetButton.PerformClick();

                            ChangeStatusLabel(statusLabel, "Pet was added.", Color.Green);
                        }
                        catch
                        {
                            ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
                        }
                    }
                    else
                    {
                        // Modify procedures`s data
                        try
                        {
                            Pet pet = context.Pets.First(p => p.Id == Convert.ToInt32(idValueLabel.Text));
                            pet.PetName = nameTb.Text;
                            pet.PetType = typeCb.SelectedItem.ToString();
                            pet.Weight = weight;
                            pet.PetFamily = familyTb.Text;
                            pet.ClientId = ((PetMasterComboItem)masterCb.SelectedItem).Id;

                            context.Pets.Update(pet);
                            context.SaveChanges();

                            int rowIndexToUpdate = -1;
                            foreach (DataGridViewRow row in petsGrid.Rows)
                            {
                                if (row.Cells[5].Value.ToString().Equals(pet.Id.ToString()))
                                {
                                    rowIndexToUpdate = row.Index;
                                    break;
                                }
                            }

                            // Update data in grid row 
                            petsGrid.Rows[rowIndexToUpdate].Cells[0].Value = pet.PetName;
                            petsGrid.CurrentRow.Cells[1].Value = ((PetMasterComboItem)masterCb.SelectedItem).ClientName;
                            petsGrid.CurrentRow.Cells[2].Value = pet.PetType;
                            petsGrid.CurrentRow.Cells[3].Value = pet.PetFamily;
                            petsGrid.CurrentRow.Cells[4].Value = pet.Weight;

                            ChangeStatusLabel(statusLabel, "Pets`s data is modified.", Color.Green);
                        }
                        catch
                        {
                            ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
                        }
                    }
                }
            };

            #endregion

            #region grid building

            if (pets.Count() > 0)
            {
                // Create columns in datagrid
                petsGrid.Columns.Add("name", "Pets Name:");
                petsGrid.Columns.Add("clName", "Master:");
                petsGrid.Columns.Add("type", "Type:");
                petsGrid.Columns.Add("family", "Family:");
                petsGrid.Columns.Add("weight", "Weight(kg):");
                petsGrid.Columns.Add("Id", "Id:");
                petsGrid.Columns[5].Visible = false;
                petsGrid.Columns.Add("remove", "");
                petsGrid.Columns.Add("modify", "");

                petsGrid.Columns[6].CellTemplate.Style.ForeColor = Color.Red;
                petsGrid.Columns[7].CellTemplate.Style.ForeColor = Color.Blue;

                petsGrid.CellClick += (object sender, DataGridViewCellEventArgs e) =>
                {
                    // Empty Cells or cells not in columns 6 or 7 READONLY
                    if ((petsGrid.CurrentCell.ColumnIndex == 6 || petsGrid.CurrentCell.ColumnIndex == 7) &&
                        petsGrid.CurrentCell.Value == null)
                        return;

                    // Column [6] - REMOVE button
                    if (petsGrid.CurrentCell.ColumnIndex == 6)
                    {
                        DialogResult dialogResult = MessageBox.Show("Remove pet from list?", "Update data", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            try
                            {
                                using (var context = new VeterinaryClinicDbContext())
                                {
                                    int petId = (int)petsGrid.CurrentRow.Cells[5].Value;
                                    context.Pets.Remove(context.Pets.First(p => p.Id == petId));
                                    context.SaveChanges();
                                    petsGrid.Rows.Remove(petsGrid.CurrentRow);

                                    if (idValueLabel.Text == petId.ToString())
                                    {
                                        idValueLabel.Text = "#";
                                    }

                                    ChangeStatusLabel(statusLabel, "Pet was removed.", Color.Green);
                                }
                            }
                            catch
                            {
                                ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
                            }
                        }
                    }
                    // Column [7] - MODIFY button
                    else if (petsGrid.CurrentCell.ColumnIndex == 7)
                    {
                        DialogResult dialogResult = MessageBox.Show("Change pet data?", "Modify data", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            idValueLabel.Text = petsGrid.CurrentRow.Cells[5].Value.ToString();
                            nameTb.Text = petsGrid.CurrentRow.Cells[0].Value.ToString();
                            familyTb.Text = petsGrid.CurrentRow.Cells[3].Value.ToString();
                            weightTb.Text = petsGrid.CurrentRow.Cells[4].Value.ToString();

                            foreach (AnimalTypes a in typeCb.Items)
                            {
                                if (a.ToString() == petsGrid.CurrentRow.Cells[2].Value.ToString())
                                {
                                    typeCb.SelectedItem = a;
                                }
                            }

                            foreach (PetMasterComboItem pm in masterCb.Items)
                            {
                                if (pm.ClientName == petsGrid.CurrentRow.Cells[1].Value.ToString())
                                {
                                    masterCb.SelectedItem = pm;
                                }
                            }
                        }
                    }
                };

                // Create rows with client info in datagrid
                foreach (Pet p in pets)
                {
                    petsGrid.Rows.Add(p.PetName,
                        p.Client.PersonName,
                        p.PetType,
                        p.PetFamily,
                        p.Weight,
                        p.Id,
                        "R", "M");
                    petsGrid.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    petsGrid.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    petsGrid.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    petsGrid.Columns[7].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                }
            }

            #endregion

            // Append all new controls to destination panel
            List<Control> fields = new List<Control>() {
                idLabel, idValueLabel,
                nameLabel, nameTb,
                typeLabel, typeCb,
                familyLabel, familyTb,
                weightLabel, weightTb,
                masterLabel, masterCb,
                submitButton, resetButton,
                statusLabel
            };

            List<Control> gridPanel = new List<Control>() {
                petsGrid
            };

            gridPanel.AddRange(searchControls);

            AppendControls(fieldsPanel, fields);
            AppendControls(allInfo, gridPanel);
            AppendControls(destinationPanel, new List<Control>() { fieldsPanel, allInfo });
        }
    }
}
