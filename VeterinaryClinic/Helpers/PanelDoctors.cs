﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VeterinaryClinic.Models;

namespace VeterinaryClinic.Helpers
{
    class PanelDoctors : PanelBase
    {
        public PanelDoctors(FlowLayoutPanel destinationPanel) : base(destinationPanel)
        {

        }

        public override void BuildPanel()
        {
            IEnumerable<Doctor> doctors;
            using (var context = new VeterinaryClinicDbContext())
            {
                doctors = context.Doctors.AsNoTracking().ToList();
            }

            #region input fields
            // Panel with work fields
            Panel fieldsPanel = CreateTextFieldsPanel();

            int locX = (int)(fieldsPanel.Width * 0.1),
                startLocY = (int)(fieldsPanel.Height * 0.1),
                tbWidth = (int)(fieldsPanel.Width * 0.8),
                stepLow = 25,
                stepFull;

            Label idLabel = new Label();
            idLabel.Location = new Point(10, 10);
            idLabel.Text = "Record №";
            idLabel.Width = 60;
            Label idValueLabel = new Label();
            idValueLabel.Location = new Point(70, 10);
            idValueLabel.Text = "#";

            Label nameLabel = new Label();
            nameLabel.Width = tbWidth;
            nameLabel.Text = "Enter first, second names:";
            nameLabel.Location = new Point(locX, startLocY);
            TextBox nameTb = new TextBox();
            nameTb.Location = new Point(locX, nameLabel.Location.Y + stepLow);
            nameTb.Width = tbWidth;
            nameTb.MaxLength = 120;

            // Calculate height of label+textBox
            stepFull = nameTb.Location.Y + nameTb.Height - nameLabel.Location.Y + 10;

            Label professionLabel = new Label();
            professionLabel.Location = new Point(locX, startLocY + stepFull);
            professionLabel.Text = "Enter profession:";
            TextBox professionTb = new TextBox();
            professionTb.Location = new Point(locX, professionLabel.Location.Y + stepLow);
            professionTb.Width = tbWidth;
            professionTb.MaxLength = 40;

            Label phoneLabel = new Label();
            phoneLabel.Location = new Point(locX, startLocY + stepFull * 2);
            phoneLabel.Text = "Enter phone:";
            MaskedTextBox phoneTb = new MaskedTextBox();
            phoneTb.Location = new Point(locX, phoneLabel.Location.Y + stepLow);
            phoneTb.Width = tbWidth;
            phoneTb.Mask = "(+38)\\000-000-00-00";

            Label emailLabel = new Label();
            emailLabel.Location = new Point(locX, startLocY + stepFull * 3);
            emailLabel.Text = "Enter email:";
            TextBox emailTb = new TextBox();
            emailTb.Location = new Point(locX, emailLabel.Location.Y + stepLow);
            emailTb.Width = tbWidth;
            #endregion

            Label statusLabel = new Label();
            statusLabel.Location = new Point(locX, startLocY + stepFull * 4);
            statusLabel.Text = statusPrefix;
            statusLabel.Width = tbWidth;

            // Panel with all doctor info
            Panel allInfo = CreateDataGridPanel();
            DataGridView doctorsGrid = CreateDataGrid(allInfo.Width, allInfo.Height);
            List<Control> searchControls = AddSearchControls(doctorsGrid, 0, "Search by name:");

            Button submitButton = new Button();
            submitButton.Name = "btnSubmit";
            submitButton.Location = new Point(locX, fieldsPanel.Height - 50);
            submitButton.Width = 60;
            submitButton.Text = "Submit";

            Button resetButton = new Button();
            resetButton.Name = "btnReset";
            resetButton.Location = new Point(locX + 80, fieldsPanel.Height - 50);
            resetButton.Width = 60;
            resetButton.Text = "Reset";

            resetButton.Click += (object sender, EventArgs e) =>
            {
                nameTb.Text = "";
                professionTb.Text = "";
                emailTb.Text = "";
                phoneTb.Text = "";
                idValueLabel.Text = "#";
            };

            submitButton.Click += (object sender, EventArgs e) =>
            {
                // Simple data validation
                if (!phoneTb.MaskCompleted || !emailTb.Text.Contains('@') || nameTb.Text.Length < 10)
                {
                    ChangeStatusLabel(statusLabel, "Some data are not correct.", Color.Red);
                    return;
                }

                using (var context = new VeterinaryClinicDbContext())
                {
                    // Check for existing id on panel with data fields
                    // # - not exists(new record) -> create new doctor and add to table
                    // <number> - exists(old record) -> modify doctors data
                    if (idValueLabel.Text == "#")
                    {
                        // Create new doctor and insert into context
                        Doctor newDoc = new Doctor()
                        {
                            PersonName = nameTb.Text,
                            Email = emailTb.Text,
                            Phone = phoneTb.Text,
                            Proffesion = professionTb.Text
                        };

                        try
                        {
                            context.Doctors.Add(newDoc);
                            context.SaveChanges();
                            doctorsGrid.Rows.Add(newDoc.PersonName, newDoc.Proffesion, newDoc.Phone, newDoc.Email, newDoc.Id, "R", "M");
                            resetButton.PerformClick();

                            ChangeStatusLabel(statusLabel, "Doctor was added.", Color.Green);
                        }
                        catch
                        {
                            ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
                        }
                    }
                    else
                    {
                        // Modify doctor`s data
                        try
                        {
                            Doctor doc = context.Doctors.First(d => d.Id == Convert.ToInt32(idValueLabel.Text));
                            doc.PersonName = nameTb.Text;
                            doc.Proffesion = professionTb.Text;
                            doc.Email = emailTb.Text;
                            doc.Phone = phoneTb.Text;
                            context.Doctors.Update(doc);
                            context.SaveChanges();

                            int rowIndexToUpdate = -1;
                            foreach (DataGridViewRow row in doctorsGrid.Rows)
                            {
                                if (row.Cells[4].Value.ToString().Equals(doc.Id.ToString()))
                                {
                                    rowIndexToUpdate = row.Index;
                                    break;
                                }
                            }

                            // Update data in grid row 
                            doctorsGrid.Rows[rowIndexToUpdate].Cells[0].Value = doc.PersonName;
                            doctorsGrid.CurrentRow.Cells[1].Value = doc.Proffesion;
                            doctorsGrid.CurrentRow.Cells[2].Value = doc.Phone;
                            doctorsGrid.CurrentRow.Cells[3].Value = doc.Email;

                            ChangeStatusLabel(statusLabel, "Doctor`s data is modified.", Color.Green);
                        }
                        catch
                        {
                            ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
                        }
                    }
                }
            };

            if (doctors.Count() > 0)
            {
                doctorsGrid.Columns.Add("name", "Doctors Name:");
                doctorsGrid.Columns.Add("profession", "Profession:");
                doctorsGrid.Columns.Add("phone", "Phone:");
                doctorsGrid.Columns.Add("email", "Email:");
                doctorsGrid.Columns.Add("Id", "Id:");
                doctorsGrid.Columns[4].Visible = false;
                doctorsGrid.Columns.Add("remove", "");
                doctorsGrid.Columns.Add("modify", "");

                doctorsGrid.Columns[5].CellTemplate.Style.ForeColor = Color.Red;
                doctorsGrid.Columns[6].CellTemplate.Style.ForeColor = Color.Blue;

                doctorsGrid.CellClick += (object sender, DataGridViewCellEventArgs e) =>
                {
                    // Empty Cells or cells not in columns 5 or 6 READONLY
                    if ((doctorsGrid.CurrentCell.ColumnIndex == 5 || doctorsGrid.CurrentCell.ColumnIndex == 6) &&
                        doctorsGrid.CurrentCell.Value == null)
                        return;

                    // Column [5] - REMOVE button
                    if (doctorsGrid.CurrentCell.ColumnIndex == 5)
                    {
                        DialogResult dialogResult = MessageBox.Show("Remove doctor from list?", "Update data", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            try
                            {
                                using (var context = new VeterinaryClinicDbContext())
                                {
                                    int docId = (int)doctorsGrid.CurrentRow.Cells[4].Value;
                                    context.Doctors.Remove(context.Doctors.First(d => d.Id == docId));
                                    context.SaveChanges();
                                    doctorsGrid.Rows.Remove(doctorsGrid.CurrentRow);

                                    if (idValueLabel.Text == docId.ToString())
                                    {
                                        idValueLabel.Text = "#";
                                    }

                                    ChangeStatusLabel(statusLabel, "Doctor was removed.", Color.Green);
                                }
                            }
                            catch
                            {
                                ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
                            }
                        }
                    }
                    // Column [6] - MODIFY button
                    else if (doctorsGrid.CurrentCell.ColumnIndex == 6)
                    {
                        DialogResult dialogResult = MessageBox.Show("Change doctors data?", "Modify data", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            idValueLabel.Text = doctorsGrid.CurrentRow.Cells[4].Value.ToString();
                            nameTb.Text = doctorsGrid.CurrentRow.Cells[0].Value.ToString();
                            professionTb.Text = doctorsGrid.CurrentRow.Cells[1].Value.ToString();
                            phoneTb.Text = doctorsGrid.CurrentRow.Cells[2].Value.ToString();
                            emailTb.Text = doctorsGrid.CurrentRow.Cells[3].Value.ToString();
                        }
                    }
                };

                // Create rows with doctor info in datagrid
                foreach (Doctor d in doctors)
                {
                    doctorsGrid.Rows.Add(d.PersonName, d.Proffesion, d.Phone, d.Email, d.Id, "R", "M");
                    doctorsGrid.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    doctorsGrid.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                }
            }

            // Apply controls to their panels
            List<Control> fields = new List<Control>()
            {
                idLabel, idValueLabel,
                nameLabel, nameTb,
                professionLabel, professionTb,
                phoneLabel, phoneTb,
                emailLabel, emailTb,
                submitButton, resetButton, statusLabel
            };

            List<Control> table = new List<Control>();
            table.Add(doctorsGrid);
            table.AddRange(searchControls);

            AppendControls(fieldsPanel, fields);
            AppendControls(allInfo, table);
            AppendControls(destinationPanel, new List<Control>() { fieldsPanel, allInfo });
        }
    }
}
