﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VeterinaryClinic.Models;


namespace VeterinaryClinic.Helpers
{
    class PanelVetProcedures : PanelBase
    {
        public PanelVetProcedures(FlowLayoutPanel destinationPanel) : base(destinationPanel)
        {

        }

        public override void BuildPanel()
        {
            IEnumerable<VetProcedure> vetProcedures;
            using (var context = new VeterinaryClinicDbContext())
            {
                vetProcedures = context.VetProcedures.AsNoTracking().ToList();
            }

            #region input fields
            // Panel with work fields
            Panel fieldsPanel = CreateTextFieldsPanel();

            int locX = (int)(fieldsPanel.Width * 0.1),
                startLocY = (int)(fieldsPanel.Height * 0.1),
                tbWidth = (int)(fieldsPanel.Width * 0.8),
                stepLow = 25,
                stepFull;

            Label idLabel = new Label();
            idLabel.Location = new Point(10, 10);
            idLabel.Text = "Record №";
            idLabel.Width = 60;
            Label idValueLabel = new Label();
            idValueLabel.Location = new Point(70, 10);
            idValueLabel.Text = "#";

            Label nameLabel = new Label();
            nameLabel.Width = tbWidth;
            nameLabel.Text = "Enter procedure name:";
            nameLabel.Location = new Point(locX, startLocY);
            TextBox nameTb = new TextBox();
            nameTb.Location = new Point(locX, nameLabel.Location.Y + stepLow);
            nameTb.Width = tbWidth;
            nameTb.MaxLength = 60;

            // Calculate height of label+textBox
            stepFull = nameTb.Location.Y + nameTb.Height - nameLabel.Location.Y + 10;

            Label durationLabel = new Label();
            durationLabel.Location = new Point(locX, startLocY + stepFull);
            durationLabel.Text = "Enter duration time(mins):";
            durationLabel.Width = tbWidth;
            TextBox durationTb = new TextBox();
            durationTb.Location = new Point(locX, durationLabel.Location.Y + stepLow);
            durationTb.Width = tbWidth;

            Label priceLabel = new Label();
            priceLabel.Location = new Point(locX, startLocY + stepFull * 2);
            priceLabel.Text = "Enter price ($):";
            TextBox priceTb = new TextBox();
            priceTb.Location = new Point(locX, priceLabel.Location.Y + stepLow);
            priceTb.Width = tbWidth;
            #endregion

            Label statusLabel = new Label();
            statusLabel.Location = new Point(locX, startLocY + stepFull * 3);
            statusLabel.Text = statusPrefix;
            statusLabel.Width = tbWidth;

            // Panel with all procedure info
            Panel allInfo = CreateDataGridPanel();
            DataGridView vetProceduresGrid = CreateDataGrid(allInfo.Width, allInfo.Height);
            List<Control> searchControls = AddSearchControls(vetProceduresGrid, 0, "Search by name:");

            #region action buttons
            Button submitButton = new Button();
            submitButton.Name = "btnSubmit";
            submitButton.Location = new Point(locX, fieldsPanel.Height - 50);
            submitButton.Width = 60;
            submitButton.Text = "Submit";

            Button resetButton = new Button();
            resetButton.Name = "btnReset";
            resetButton.Location = new Point(locX + 80, fieldsPanel.Height - 50);
            resetButton.Width = 60;
            resetButton.Text = "Reset";

            resetButton.Click += (object sender, EventArgs e) =>
            {
                nameTb.Text = "";
                durationTb.Text = "";
                priceTb.Text = "";
                idValueLabel.Text = "#";
            };

            submitButton.Click += (object sender, EventArgs e) =>
            {
                // Simple data validation
                decimal durationTime = 0;
                decimal price = 0;

                if (nameTb.Text.Length < 5 ||
                    !decimal.TryParse(durationTb.Text, out durationTime) ||
                    !decimal.TryParse(priceTb.Text, out price))
                {
                    ChangeStatusLabel(statusLabel, "Some data are not correct.", Color.Red);
                    return;
                }

                using (var context = new VeterinaryClinicDbContext())
                {
                    // Check for existing id on panel with data fields
                    // # - not exists(new record) -> create new procedure and add to table
                    // <number> - exists(old record) -> modify procedure data
                    if (idValueLabel.Text == "#")
                    {
                        // Create new procedure and insert into context
                        VetProcedure newProcedure = new VetProcedure()
                        {
                            ProcedureName = nameTb.Text,
                            Duration = durationTime,
                            Price = price,
                        };

                        try
                        {
                            context.VetProcedures.Add(newProcedure);
                            context.SaveChanges();
                            vetProceduresGrid.Rows.Add(newProcedure.ProcedureName,
                                newProcedure.Duration.ToString("0.00"),
                                newProcedure.Price.ToString("0.00"),
                                newProcedure.Id,
                                "R", "M");
                            resetButton.PerformClick();

                            ChangeStatusLabel(statusLabel, "Procedure was added.", Color.Green);
                        }
                        catch
                        {
                            ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
                        }
                    }
                    else
                    {
                        // Modify procedures`s data
                        try
                        {
                            VetProcedure vetProc = context.VetProcedures.First(vp => vp.Id == Convert.ToInt32(idValueLabel.Text));
                            vetProc.ProcedureName = nameTb.Text;
                            vetProc.Duration = durationTime;
                            vetProc.Price = price;
                            context.VetProcedures.Update(vetProc);
                            context.SaveChanges();

                            int rowIndexToUpdate = -1;
                            foreach (DataGridViewRow row in vetProceduresGrid.Rows)
                            {
                                if (row.Cells[3].Value.ToString().Equals(vetProc.Id.ToString()))
                                {
                                    rowIndexToUpdate = row.Index;
                                    break;
                                }
                            }

                            // Update data in grid row 
                            vetProceduresGrid.Rows[rowIndexToUpdate].Cells[0].Value = vetProc.ProcedureName;
                            vetProceduresGrid.CurrentRow.Cells[1].Value = vetProc.Duration.ToString("0.00");
                            vetProceduresGrid.CurrentRow.Cells[2].Value = vetProc.Price.ToString("0.00");

                            ChangeStatusLabel(statusLabel, "Clients`s data is modified.", Color.Green);
                        }
                        catch
                        {
                            ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
                        }
                    }
                }
            };

            #endregion

            #region grid building

            if (vetProcedures.Count() > 0)
            {
                // Create columns in datagrid
                vetProceduresGrid.Columns.Add("name", "Procedures Name:");
                vetProceduresGrid.Columns.Add("duration", "Duration:");
                vetProceduresGrid.Columns.Add("price", "Price:");
                vetProceduresGrid.Columns.Add("Id", "Id:");
                vetProceduresGrid.Columns[3].Visible = false;
                vetProceduresGrid.Columns.Add("remove", "");
                vetProceduresGrid.Columns.Add("modify", "");

                vetProceduresGrid.Columns[4].CellTemplate.Style.ForeColor = Color.Red;
                vetProceduresGrid.Columns[5].CellTemplate.Style.ForeColor = Color.Blue;

                vetProceduresGrid.CellClick += (object sender, DataGridViewCellEventArgs e) =>
                {
                    // Empty Cells or cells not in columns 4 or 5 READONLY
                    if ((vetProceduresGrid.CurrentCell.ColumnIndex == 4 || vetProceduresGrid.CurrentCell.ColumnIndex == 5) &&
                        vetProceduresGrid.CurrentCell.Value == null)
                        return;

                    // Column [4] - REMOVE button
                    if (vetProceduresGrid.CurrentCell.ColumnIndex == 4)
                    {
                        DialogResult dialogResult = MessageBox.Show("Remove procedure from list?", "Update data", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            try
                            {
                                using (var context = new VeterinaryClinicDbContext())
                                {
                                    int clId = (int)vetProceduresGrid.CurrentRow.Cells[3].Value;
                                    context.VetProcedures.Remove(context.VetProcedures.First(d => d.Id == clId));
                                    context.SaveChanges();
                                    vetProceduresGrid.Rows.Remove(vetProceduresGrid.CurrentRow);

                                    if (idValueLabel.Text == clId.ToString())
                                    {
                                        idValueLabel.Text = "#";
                                    }

                                    ChangeStatusLabel(statusLabel, "Procedure was removed.", Color.Green);
                                }
                            }
                            catch
                            {
                                ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
                            }
                        }
                    }
                    // Column [5] - MODIFY button
                    else if (vetProceduresGrid.CurrentCell.ColumnIndex == 5)
                    {
                        DialogResult dialogResult = MessageBox.Show("Change procedure data?", "Modify data", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            idValueLabel.Text = vetProceduresGrid.CurrentRow.Cells[3].Value.ToString();
                            nameTb.Text = vetProceduresGrid.CurrentRow.Cells[0].Value.ToString();
                            durationTb.Text = vetProceduresGrid.CurrentRow.Cells[1].Value.ToString();
                            priceTb.Text = vetProceduresGrid.CurrentRow.Cells[2].Value.ToString();
                        }
                    }
                };

                // Create rows with client info in datagrid
                foreach (VetProcedure vp in vetProcedures)
                {
                    vetProceduresGrid.Rows.Add(vp.ProcedureName, vp.Duration, vp.Price, vp.Id, "R", "M");
                    vetProceduresGrid.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    vetProceduresGrid.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                }
            }

            #endregion

            // Append all new controls to destination panel
            List<Control> fields = new List<Control>() {
                idLabel, idValueLabel,
                nameLabel, nameTb,
                durationLabel, durationTb,
                priceLabel, priceTb,
                submitButton, resetButton,
                statusLabel
            };

            List<Control> gridPanel = new List<Control>() {
                vetProceduresGrid
            };

            gridPanel.AddRange(searchControls);

            AppendControls(fieldsPanel, fields);
            AppendControls(allInfo, gridPanel);
            AppendControls(destinationPanel, new List<Control>() { fieldsPanel, allInfo });
        }
    }
}
