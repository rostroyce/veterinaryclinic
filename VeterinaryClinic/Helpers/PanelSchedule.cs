﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VeterinaryClinic.Models;
using VeterinaryClinic.Models.ViewModels;
using VeterinaryClinic.Views.SeanceWindow;

namespace VeterinaryClinic.Helpers
{
    class PanelSchedule : PanelBase
    {
        private readonly Color activeBtn = Color.Pink;
        private readonly Color simpleBtn = Color.LightGray;

        public PanelSchedule(FlowLayoutPanel destinationPanel) : base(destinationPanel)
        {
            this.destinationPanel = destinationPanel;
        }

        public override void BuildPanel()
        {
            Panel dates = CreateTextFieldsPanel();
            Panel seancesInfo = CreateDataGridPanel();
            DataGridView seancesGrid = new DataGridView();

            int currentYear = DateTime.Now.Year,
                currentMonth = DateTime.Now.Month,
                currentDay = DateTime.Now.Day;

            int days = DateTime.DaysInMonth(currentYear, currentMonth);

            Label dateLabel = new Label();
            dateLabel.Text = "Date:";
            dateLabel.Location = new Point(10, 10);
            dateLabel.Width = 60;
            ComboBox yearCb = new ComboBox();
            yearCb.Location = new Point(75, 10);
            yearCb.Width = 70;
            yearCb.DropDownStyle = ComboBoxStyle.DropDownList;

            // Add years to combobox
            for (int i = 1; i < 10; i++)
            {
                int year = 2020 + i;
                yearCb.Items.Add(year.ToString());

                if (year == currentYear)
                {
                    yearCb.SelectedIndex = i - 1;
                }
            }

            ComboBox monthCb = new ComboBox();
            monthCb.Location = new Point(155, 10);
            monthCb.Width = 100;
            monthCb.DropDownStyle = ComboBoxStyle.DropDownList;

            // Add monthes to combobox
            foreach (Monthes month in Enum.GetValues(typeof(Monthes)))
            {
                int index = monthCb.Items.Add(month);

                if (month == (Monthes)currentMonth)
                {
                    monthCb.SelectedIndex = index;
                }
            }

            FlowLayoutPanel dayPanel = new FlowLayoutPanel();
            dayPanel.Width = (int)(dates.Width * 0.85);
            dayPanel.Height = (int)(dates.Height * 0.60);
            dayPanel.Location = new Point(20, 60);

            yearCb.SelectedIndexChanged += (object sender, EventArgs e) =>
            {
                monthCb.SelectedIndex = 0;

                BuildDayButtons(
                    dayPanel,
                    Convert.ToInt32(yearCb.SelectedItem.ToString()),
                    Convert.ToInt32(((Monthes)monthCb.SelectedItem)),
                    seancesGrid);

                ((Button)dayPanel.Controls[0]).PerformClick();
            };

            monthCb.SelectedIndexChanged += (object sender, EventArgs e) =>
            {
                BuildDayButtons(
                    dayPanel,
                    Convert.ToInt32(yearCb.SelectedItem.ToString()),
                    Convert.ToInt32(((Monthes)monthCb.SelectedItem)),
                    seancesGrid);

                ((Button)dayPanel.Controls[0]).PerformClick();
            };

            int curYear = Convert.ToInt32(yearCb.SelectedItem);
            int curMonth = (int)(Monthes)monthCb.SelectedItem;
            BuildDayButtons(dayPanel, curYear, curMonth, seancesGrid);
            // Show seances for today
            ((Button)dayPanel.Controls[DateTime.Now.Day - 1]).PerformClick();

            Label statusLabel = new Label();
            statusLabel.Location = new Point(10, dayPanel.Location.Y + dayPanel.Height + 45);
            statusLabel.Text = statusPrefix;
            statusLabel.Width = dayPanel.Width;

            seancesGrid.Width = (int)(seancesInfo.Width * 0.95);
            seancesGrid.Height = (int)(seancesInfo.Height * 0.75);
            seancesGrid.Location = new Point(10, 10);
            seancesGrid.ReadOnly = true;
            seancesGrid.AllowUserToResizeRows = false;
            seancesGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            seancesGrid.AllowUserToAddRows = false;

            // Create columns in datagrid
            seancesGrid.Columns.Add("seanceId", "#");
            seancesGrid.Columns.Add("doctor", "Doctor:");
            seancesGrid.Columns.Add("client", "Client:");
            seancesGrid.Columns.Add("petType", "Pet:");
            seancesGrid.Columns.Add("startIn", "Start:");
            seancesGrid.Columns.Add("endIn", "End:");
            seancesGrid.Columns.Add("remove", "");
            seancesGrid.Columns.Add("modify", "");
            seancesGrid.Columns.Add("info", "");
            seancesGrid.Columns.Add("seanceData", "");
            seancesGrid.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            seancesGrid.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            seancesGrid.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            seancesGrid.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            seancesGrid.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            seancesGrid.Columns[7].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            seancesGrid.Columns[8].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            seancesGrid.Columns[6].CellTemplate.Style.ForeColor = Color.Red;
            seancesGrid.Columns[7].CellTemplate.Style.ForeColor = Color.Blue;
            seancesGrid.Columns[8].CellTemplate.Style.ForeColor = Color.Orange;

            seancesGrid.Columns[9].Visible = false;

            seancesGrid.CellClick += (object sender, DataGridViewCellEventArgs e) =>
            {
                // Empty Cells or cells not in columns 5 or 6 READONLY
                if (seancesGrid.CurrentCell == null ||
                    seancesGrid.CurrentCell.ColumnIndex < 6
                )
                    return;

                // Column [6] - REMOVE button
                if (seancesGrid.CurrentCell.ColumnIndex == 6)
                {
                    DialogResult dialogResult = MessageBox.Show("Remove seance from list?", "Update data", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        try
                        {
                            using (var context = new VeterinaryClinicDbContext())
                            {
                                int seanceId = (int)seancesGrid.CurrentRow.Cells[0].Value;
                                context.Seances.Remove(context.Seances.First(d => d.Id == seanceId));
                                context.SaveChanges();
                                seancesGrid.Rows.Remove(seancesGrid.CurrentRow);

                                ChangeStatusLabel(statusLabel, "Seance was cancelled.", Color.Green);
                            }
                        }
                        catch
                        {
                            ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
                        }
                    }
                }
                // Column [7] - MODIFY button
                else if (seancesGrid.CurrentCell.ColumnIndex == 7)
                {
                    DateTime seanceStartDate = Convert.ToDateTime(seancesGrid.CurrentRow.Cells["seanceData"].Value);

                    if (seanceStartDate < DateTime.Now)
                    {
                        MessageBox.Show("You cannot modify seance, that was already started.");
                    }
                    else
                    {
                        DialogResult dialogResult = MessageBox.Show("Change seance data?", "Modify data", MessageBoxButtons.YesNo);

                        if (dialogResult == DialogResult.Yes)
                        {
                            using (var seanceForm = new SeanceWindow(Convert.ToInt32(seancesGrid.CurrentRow.Cells[0].Value), true))
                            {
                                DialogResult actWithSeance = seanceForm.ShowDialog();

                                // Change current window data after successfull seance modification
                                if (actWithSeance == DialogResult.OK)
                                {
                                    ChangeComboboxData(yearCb, monthCb, seanceForm.ResultDate, dayPanel);
                                }
                            }
                        }
                    }
                }
                // Column [8] - INFO button
                else if (seancesGrid.CurrentCell.ColumnIndex == 8)
                {
                    DialogResult dialogResult = MessageBox.Show("Open seance data?", "Information.", MessageBoxButtons.YesNo);

                    if (dialogResult == DialogResult.Yes)
                    {
                        using (var seanceForm = new SeanceWindow(Convert.ToInt32(seancesGrid.CurrentRow.Cells[0].Value), false))
                        {
                            DialogResult actWithSeance = seanceForm.ShowDialog();

                            // Change current window data after successfull seance modification
                            if (actWithSeance == DialogResult.OK)
                            {
                                ChangeComboboxData(yearCb, monthCb, seanceForm.ResultDate, dayPanel);
                            }
                        }
                    }
                }
            };

            Button addSeanceBtn = new Button();
            addSeanceBtn.Location = new Point(20, seancesGrid.Location.Y + seancesGrid.Height + 25);
            addSeanceBtn.Text = "New Seance";
            addSeanceBtn.Width = 100;
            addSeanceBtn.Height = 60;
            addSeanceBtn.Click += (object sender, EventArgs e) =>
            {
                using (var seanceForm = new SeanceWindow(-1, true))
                {
                    DialogResult actWithSeance = seanceForm.ShowDialog();

                    // Change current window data after successfull seance modification
                    if (actWithSeance == DialogResult.OK)
                    {
                        ChangeComboboxData(yearCb, monthCb, seanceForm.ResultDate, dayPanel);
                    }
                }
            };

            dates.Controls.Add(dateLabel);
            dates.Controls.Add(yearCb);
            dates.Controls.Add(monthCb);
            dates.Controls.Add(dayPanel);
            dates.Controls.Add(statusLabel);
            seancesInfo.Controls.Add(seancesGrid);
            seancesInfo.Controls.Add(addSeanceBtn);

            destinationPanel.Controls.Clear();
            destinationPanel.Controls.Add(dates);
            destinationPanel.Controls.Add(seancesInfo);
        }

        private void BuildDayButtons(FlowLayoutPanel buttonPanel, int year, int month, DataGridView seancesGrid)
        {
            buttonPanel.Controls.Clear();

            for (int i = 0; i < DateTime.DaysInMonth(year, month); i++)
            {
                Button dayButton = new Button();
                dayButton.Text = (i + 1).ToString();
                dayButton.Size = new Size(34, 34);
                dayButton.BackColor = simpleBtn;
                buttonPanel.Controls.Add(dayButton);

                dayButton.Click += (object sender, EventArgs e) =>
                {
                    foreach (Button b in buttonPanel.Controls)
                    {
                        b.BackColor = simpleBtn;
                    }

                    dayButton.BackColor = activeBtn;

                    var chosenDate = new DateTime(
                        year,
                        month,
                        Convert.ToInt32(dayButton.Text)
                        );

                    GetSeancesAsync(chosenDate, seancesGrid);
                };
            }
        }

        private async void GetSeancesAsync(DateTime date, DataGridView seancesGrid)
        {
            SynchronizationContext UiContext = SynchronizationContext.Current;

            await Task.Run(() =>
            {
                using (var context = new VeterinaryClinicDbContext())
                {
                    var seances = context.Seances
                        .Where(s => s.SeanceDate.Date == date.Date)
                        .OrderBy(s => s.SeanceDate)
                        .Select(s => new SeanceViewModel
                        {
                            SeanceId = s.Id,
                            DoctorName = s.Doctor.PersonName,
                            ClientName = s.Pet.Client.PersonName,
                            PetType = s.Pet.PetType,
                            StartsIn = s.SeanceDate.ToString(),
                            Duration = s.VetProcedures.Sum(p => p.Duration)
                        })
                        .AsNoTracking()
                        .ToList();

                    UiContext.Send(
                        (c) => {
                            BuildSeancesGrid(seances, seancesGrid);
                        },
                        null);
                }
            });
        }

        private void BuildSeancesGrid(List<SeanceViewModel> seances, DataGridView seancesGrid)
        {
            try
            {
                seancesGrid.Rows.Clear();

                foreach (SeanceViewModel model in seances)
                {
                    var startTime = Convert.ToDateTime(model.StartsIn).ToString("HH:mm");
                    var endTime = Convert.ToDateTime(model.StartsIn)
                        .AddMinutes(Convert.ToDouble(model.Duration))
                        .ToString("HH:mm");

                    seancesGrid.Rows.Add(model.SeanceId,
                        model.DoctorName,
                        model.ClientName,
                        model.PetType,
                        startTime,
                        endTime,
                        "R", "M", "I",
                        model.StartsIn
                        );
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void ChangeComboboxData(ComboBox yearCb, ComboBox monthCb, DateTime date, FlowLayoutPanel buttonPanel)
        {
            foreach (var item in yearCb.Items)
            {
                if (item.ToString() == date.Year.ToString())
                {
                    yearCb.SelectedItem = item;
                    break;
                }
            }

            foreach (Monthes item in monthCb.Items)
            {
                if (Convert.ToInt32(item) == date.Month)
                {
                    monthCb.SelectedItem = item;
                    break;
                }
            }

            ((Button)buttonPanel.Controls[date.Day - 1]).PerformClick();
        }
    }
}
