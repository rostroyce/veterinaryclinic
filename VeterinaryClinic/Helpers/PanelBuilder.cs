﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VeterinaryClinic.Models;
using System.Drawing;
using System.Globalization;
using VeterinaryClinic.Components;
using Microsoft.EntityFrameworkCore;
using VeterinaryClinic.Models.ViewModels;
using VeterinaryClinic.Views.SeanceWindow;
using System.Threading;

namespace VeterinaryClinic.Helpers
{
    internal class PanelBuilder
    {
        FlowLayoutPanel destinationPanel;

        public PanelBuilder (FlowLayoutPanel destinationPanel)
        {
            this.destinationPanel = destinationPanel;
        }

        public void Build(Navigation currentNavigation)
        {
            switch (currentNavigation)
            {
                case Navigation.Schedule:
                    new PanelSchedule(destinationPanel).BuildPanel();
                    break;
                case Navigation.Doctor:
                    new PanelDoctors(destinationPanel).BuildPanel();
                    break;
                case Navigation.Client:
                    new PanelClients(destinationPanel).BuildPanel();
                    break;
                case Navigation.Pet:
                    new PanelPets(destinationPanel).BuildPanel();
                    break;
                case Navigation.VetProcedure:
                    new PanelVetProcedures(destinationPanel).BuildPanel();
                    break;
            }
        }

        //#region base content panels
        //private Panel CreateTextFieldsPanel()
        //{
        //    // Panel with work fields
        //    Panel leftFieldsPanel = new Panel();
        //    leftFieldsPanel.Location = new Point(10, 10);
        //    leftFieldsPanel.Width = destinationPanel.Width / 3 - 20;
        //    leftFieldsPanel.Height = (int)(destinationPanel.Height * 0.85);
        //    //leftFieldsPanel.BackColor = Color.SandyBrown;
        //    leftFieldsPanel.BorderStyle = BorderStyle.FixedSingle;

        //    return leftFieldsPanel;
        //}

        //private Panel CreateDataGridPanel()
        //{
        //    Panel rightGridPanel = new Panel();
        //    rightGridPanel.Location = new Point((int)(destinationPanel.Width * 0.65), 10);
        //    rightGridPanel.Width = (int)(destinationPanel.Width * 0.65) - 20;
        //    rightGridPanel.Height = (int)(destinationPanel.Height * 0.85);
        //    //rightGridPanel.BackColor = Color.AliceBlue;

        //    return rightGridPanel;
        //}

        //private DataGridView CreateDataGrid(int parentWidth, int parentHeight)
        //{
        //    DataGridView grid = new DataGridView();
        //    grid.Width = (int)(parentWidth * 0.95);
        //    grid.Height = (int)(parentHeight * 0.85);
        //    grid.Location = new Point(10, 10);
        //    grid.ReadOnly = true;
        //    grid.AllowUserToResizeRows = false;
        //    grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        //    grid.AllowUserToAddRows = false;

        //    return grid;
        //}
        ///*
        // * dataGrid - current DataGridView
        // * searchCellIndex - index of cell where we have to search
        // * searchText - text for label, at left side of search textbox
        // */
        //private List<Control> AddSearchControls(DataGridView dataGrid, int searchCellIndex, string searchText)
        //{
        //    List<Control> searchControls = new List<Control>();

        //    // Search components
        //    Label searchLabel = new Label();
        //    searchLabel.Text = searchText;
        //    searchLabel.Location = new Point(10, dataGrid.Location.Y + dataGrid.Height + 30);
        //    searchLabel.Width = 100;
        //    searchControls.Add(searchLabel);
        //    TextBox searchTb = new TextBox();
        //    searchTb.Location = new Point(
        //        searchLabel.Location.X + searchLabel.Width + 10,
        //        searchLabel.Location.Y);
        //    searchTb.Width = 100;
        //    searchControls.Add(searchTb);
        //    Button searchButton = new Button();
        //    searchButton.Text = "Search";
        //    searchButton.Location = new Point(
        //        searchTb.Location.X + searchTb.Width + 10,
        //        searchTb.Location.Y);
        //    searchControls.Add(searchButton);
        //    Button searchResetButton = new Button();
        //    searchResetButton.Text = "Reset";
        //    searchResetButton.Location = new Point(
        //        searchButton.Location.X + searchButton.Width + 10,
        //        searchButton.Location.Y);
        //    searchControls.Add(searchResetButton);

        //    searchButton.Click += (object sender, EventArgs e) =>
        //    {
        //        if (dataGrid.Rows.Count > 0 && searchTb.Text.Trim() != "")
        //        {
        //            foreach (DataGridViewRow row in dataGrid.Rows)
        //            {
        //                bool hasString = row.Cells[searchCellIndex].Value.ToString().ToLower().Contains(searchTb.Text.ToLower());
        //                if (!hasString)
        //                {
        //                    row.Visible = false;
        //                }
        //                else
        //                {
        //                    row.Visible = true;
        //                }
        //            }
        //        }
        //        else if (searchTb.Text.Trim() == "")
        //        {
        //            foreach (DataGridViewRow row in dataGrid.Rows)
        //            {
        //                row.Visible = true;
        //            }
        //        }
        //    };

        //    searchResetButton.Click += (object sender, EventArgs e) =>
        //    {
        //        if (dataGrid.Rows.Count > 0)
        //        {
        //            foreach (DataGridViewRow row in dataGrid.Rows)
        //            {
        //                row.Visible = true;
        //            }
        //        }
        //    };

        //    return searchControls;
        //}
        //#endregion

        //#region Schedule Panel

        //public void CreateSchedulePanel()
        //{
        //    Panel dates = CreateTextFieldsPanel();
        //    Panel seancesInfo = CreateDataGridPanel();
        //    DataGridView seancesGrid = new DataGridView();

        //    int currentYear = DateTime.Now.Year,
        //        currentMonth = DateTime.Now.Month,
        //        currentDay = DateTime.Now.Day;

        //    int days = DateTime.DaysInMonth(currentYear, currentMonth);

        //    Label dateLabel = new Label();
        //    dateLabel.Text = "Date:";
        //    dateLabel.Location = new Point(10, 10);
        //    dateLabel.Width = 60;
        //    ComboBox yearCb = new ComboBox();
        //    yearCb.Location = new Point(75, 10);
        //    yearCb.Width = 70;
        //    yearCb.DropDownStyle = ComboBoxStyle.DropDownList;

        //    // Add years to combobox
        //    for (int i = 1; i < 10; i++)
        //    {
        //        int year = 2020 + i;
        //        yearCb.Items.Add(year.ToString());

        //        if (year == currentYear)
        //        {
        //            yearCb.SelectedIndex = i - 1;
        //        }
        //    }

        //    ComboBox monthCb = new ComboBox();
        //    monthCb.Location = new Point(155, 10);
        //    monthCb.Width = 100;
        //    monthCb.DropDownStyle = ComboBoxStyle.DropDownList;

        //    // Add monthes to combobox
        //    foreach (Monthes month in Enum.GetValues(typeof(Monthes)))
        //    {
        //        int index = monthCb.Items.Add(month);

        //        if (month == (Monthes)currentMonth)
        //        {
        //            monthCb.SelectedIndex = index;
        //        }
        //    }

        //    FlowLayoutPanel dayPanel = new FlowLayoutPanel();
        //    dayPanel.Width = (int)(dates.Width * 0.85);
        //    dayPanel.Height = (int)(dates.Height * 0.60);
        //    dayPanel.Location = new Point(20, 60);

        //    yearCb.SelectedIndexChanged += (object sender, EventArgs e) =>
        //    {
        //        monthCb.SelectedIndex = 0;

        //        BuildDayButtons(
        //            dayPanel,
        //            Convert.ToInt32(yearCb.SelectedItem.ToString()),
        //            Convert.ToInt32(((Monthes)monthCb.SelectedItem)),
        //            seancesGrid);

        //        ((Button)dayPanel.Controls[0]).PerformClick();
        //    };

        //    monthCb.SelectedIndexChanged += (object sender, EventArgs e) =>
        //    {
        //        BuildDayButtons(
        //            dayPanel,
        //            Convert.ToInt32(yearCb.SelectedItem.ToString()),
        //            Convert.ToInt32(((Monthes)monthCb.SelectedItem)),
        //            seancesGrid);

        //        ((Button)dayPanel.Controls[0]).PerformClick();
        //    };

        //    int curYear = Convert.ToInt32(yearCb.SelectedItem);
        //    int curMonth = (int)(Monthes)monthCb.SelectedItem;

        //    BuildDayButtons(dayPanel, curYear, curMonth, seancesGrid);
        //    // Show seances for today
        //    ((Button)dayPanel.Controls[DateTime.Now.Day - 1]).PerformClick();

        //    Label statusLabel = new Label();
        //    statusLabel.Location = new Point(10, dayPanel.Location.Y + dayPanel.Height + 45);
        //    statusLabel.Text = statusPrefix;
        //    statusLabel.Width = dayPanel.Width;

        //    seancesGrid.Width = (int)(seancesInfo.Width * 0.95);
        //    seancesGrid.Height = (int)(seancesInfo.Height * 0.75);
        //    seancesGrid.Location = new Point(10, 10);
        //    seancesGrid.ReadOnly = true;
        //    seancesGrid.AllowUserToResizeRows = false;
        //    seancesGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        //    seancesGrid.AllowUserToAddRows = false;

        //    // Create columns in datagrid
        //    seancesGrid.Columns.Add("seanceId", "#");
        //    seancesGrid.Columns.Add("doctor", "Doctor:");
        //    seancesGrid.Columns.Add("client", "Client:");
        //    seancesGrid.Columns.Add("petType", "Pet:");
        //    seancesGrid.Columns.Add("startIn", "Start:");
        //    seancesGrid.Columns.Add("endIn", "End:");
        //    seancesGrid.Columns.Add("remove", "");
        //    seancesGrid.Columns.Add("modify", "");
        //    seancesGrid.Columns.Add("info", "");
        //    seancesGrid.Columns.Add("seanceData", "");
        //    seancesGrid.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //    seancesGrid.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //    seancesGrid.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //    seancesGrid.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //    seancesGrid.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //    seancesGrid.Columns[7].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //    seancesGrid.Columns[8].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

        //    seancesGrid.Columns[6].CellTemplate.Style.ForeColor = Color.Red;
        //    seancesGrid.Columns[7].CellTemplate.Style.ForeColor = Color.Blue;
        //    seancesGrid.Columns[8].CellTemplate.Style.ForeColor = Color.Orange;

        //    seancesGrid.Columns[9].Visible = false;

        //    seancesGrid.CellClick += (object sender, DataGridViewCellEventArgs e) =>
        //    {
        //        // Empty Cells or cells not in columns 5 or 6 READONLY
        //        if (seancesGrid.CurrentCell == null ||
        //            seancesGrid.CurrentCell.ColumnIndex < 6
        //        )
        //            return;

        //        // Column [6] - REMOVE button
        //        if (seancesGrid.CurrentCell.ColumnIndex == 6)
        //        {
        //            DialogResult dialogResult = MessageBox.Show("Remove seance from list?", "Update data", MessageBoxButtons.YesNo);
        //            if (dialogResult == DialogResult.Yes)
        //            {
        //                try
        //                {
        //                    using (var context = new VeterinaryClinicDbContext())
        //                    {
        //                        int seanceId = (int)seancesGrid.CurrentRow.Cells[0].Value;
        //                        context.Seances.Remove(context.Seances.First(d => d.Id == seanceId));
        //                        context.SaveChanges();
        //                        seancesGrid.Rows.Remove(seancesGrid.CurrentRow);

        //                        ChangeStatusLabel(statusLabel, "Seance was cancelled.", Color.Green);
        //                    }
        //                }
        //                catch
        //                {
        //                    ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
        //                }
        //            }
        //        }
        //        // Column [7] - MODIFY button
        //        else if (seancesGrid.CurrentCell.ColumnIndex == 7)
        //        {
        //            DateTime seanceStartDate = Convert.ToDateTime(seancesGrid.CurrentRow.Cells["seanceData"].Value);

        //            if (seanceStartDate < DateTime.Now)
        //            {
        //                MessageBox.Show("You cannot modify seance, that was already started.");
        //            } 
        //            else
        //            {
        //                DialogResult dialogResult = MessageBox.Show("Change seance data?", "Modify data", MessageBoxButtons.YesNo);

        //                if (dialogResult == DialogResult.Yes)
        //                {
        //                    using (var seanceForm = new SeanceWindow(Convert.ToInt32(seancesGrid.CurrentRow.Cells[0].Value), true))
        //                    {
        //                        DialogResult actWithSeance = seanceForm.ShowDialog();

        //                        // Change current window data after successfull seance modification
        //                        if (actWithSeance == DialogResult.OK)
        //                        {
        //                            ChangeComboboxData(yearCb, monthCb, seanceForm.ResultDate, dayPanel);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        // Column [8] - INFO button
        //        else if (seancesGrid.CurrentCell.ColumnIndex == 8)
        //        {
        //            DialogResult dialogResult = MessageBox.Show("Open seance data?", "Information.", MessageBoxButtons.YesNo);

        //            if (dialogResult == DialogResult.Yes)
        //            {
        //                using (var seanceForm = new SeanceWindow(Convert.ToInt32(seancesGrid.CurrentRow.Cells[0].Value), false))
        //                {
        //                    DialogResult actWithSeance = seanceForm.ShowDialog();

        //                    // Change current window data after successfull seance modification
        //                    if (actWithSeance == DialogResult.OK)
        //                    {
        //                        ChangeComboboxData(yearCb, monthCb, seanceForm.ResultDate, dayPanel);
        //                    }
        //                }
        //            }
        //        }
        //    };

        //    Button addSeanceBtn = new Button();
        //    addSeanceBtn.Location = new Point(20, seancesGrid.Location.Y + seancesGrid.Height + 25);
        //    addSeanceBtn.Text = "New Seance";
        //    addSeanceBtn.Width = 100;
        //    addSeanceBtn.Height = 60;
        //    addSeanceBtn.Click += (object sender, EventArgs e) =>
        //    {
        //        using (var seanceForm = new SeanceWindow(-1, true))
        //        {
        //            DialogResult actWithSeance = seanceForm.ShowDialog();

        //            // Change current window data after successfull seance modification
        //            if (actWithSeance == DialogResult.OK)
        //            {
        //                ChangeComboboxData(yearCb, monthCb, seanceForm.ResultDate, dayPanel);
        //            }
        //        }
        //    };

        //    dates.Controls.Add(dateLabel);
        //    dates.Controls.Add(yearCb);
        //    dates.Controls.Add(monthCb);
        //    dates.Controls.Add(dayPanel);
        //    dates.Controls.Add(statusLabel);
        //    seancesInfo.Controls.Add(seancesGrid);
        //    seancesInfo.Controls.Add(addSeanceBtn);

        //    destinationPanel.Controls.Clear();
        //    destinationPanel.Controls.Add(dates);
        //    destinationPanel.Controls.Add(seancesInfo);
        //}

        //private void BuildDayButtons(FlowLayoutPanel buttonPanel, int year, int month, DataGridView seancesGrid)
        //{
        //    buttonPanel.Controls.Clear();

        //    for (int i = 0; i < DateTime.DaysInMonth(year, month); i++)
        //    {
        //        Button dayButton = new Button();
        //        dayButton.Text = (i + 1).ToString();
        //        dayButton.Size = new Size(34, 34);
        //        dayButton.BackColor = simpleBtn;
        //        buttonPanel.Controls.Add(dayButton);

        //        dayButton.Click += (object sender, EventArgs e) =>
        //        {
        //            foreach (Button b in buttonPanel.Controls)
        //            {
        //                b.BackColor = simpleBtn;
        //            }

        //            dayButton.BackColor = activeBtn;

        //            var chosenDate = new DateTime(
        //                year,
        //                month,
        //                Convert.ToInt32(dayButton.Text)
        //                );

        //            GetSeancesAsync(chosenDate, seancesGrid);
        //        };
        //    }
        //}

        //private async void GetSeancesAsync(DateTime date, DataGridView seancesGrid)
        //{
        //    SynchronizationContext UiContext = SynchronizationContext.Current;

        //    await Task.Run(() =>
        //    {
        //        using (var context = new VeterinaryClinicDbContext())
        //        {
        //            var seances = context.Seances
        //                .Where(s => s.SeanceDate.Date == date.Date)
        //                .OrderBy(s => s.SeanceDate)
        //                .Select(s => new SeanceViewModel
        //                {
        //                    SeanceId = s.Id,
        //                    DoctorName = s.Doctor.PersonName,
        //                    ClientName = s.Pet.Client.PersonName,
        //                    PetType = s.Pet.PetType,
        //                    StartsIn = s.SeanceDate.ToString(),
        //                    Duration = s.VetProcedures.Sum(p => p.Duration)
        //                })
        //                .AsNoTracking()
        //                .ToList();

        //            UiContext.Send(
        //                (c) => {
        //                    BuildSeancesGrid(seances, seancesGrid);
        //                },
        //                null);
        //        }
        //    });
        //}

        //private void BuildSeancesGrid(List<SeanceViewModel> seances, DataGridView seancesGrid)
        //{
        //    try
        //    {
        //        seancesGrid.Rows.Clear();

        //        foreach (SeanceViewModel model in seances)
        //        {
        //            var startTime = Convert.ToDateTime(model.StartsIn).ToString("HH:mm");
        //            var endTime = Convert.ToDateTime(model.StartsIn)
        //                .AddMinutes(Convert.ToDouble(model.Duration))
        //                .ToString("HH:mm");

        //            seancesGrid.Rows.Add(model.SeanceId,
        //                model.DoctorName,
        //                model.ClientName,
        //                model.PetType,
        //                startTime,
        //                endTime,
        //                "R", "M", "I",
        //                model.StartsIn
        //                );
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message);
        //    }
        //}

        //public void ChangeComboboxData(ComboBox yearCb, ComboBox monthCb, DateTime date, FlowLayoutPanel buttonPanel)
        //{
        //    foreach (var item in yearCb.Items)
        //    {
        //        if (item.ToString() == date.Year.ToString())
        //        {
        //            yearCb.SelectedItem = item;
        //            break;
        //        }
        //    }

        //    foreach (Monthes item in monthCb.Items)
        //    {
        //        if (Convert.ToInt32(item) == date.Month)
        //        {
        //            monthCb.SelectedItem = item;
        //            break;
        //        }
        //    }

        //    ((Button)buttonPanel.Controls[date.Day - 1]).PerformClick();
        //}
        //#endregion

        //#region Doctor Panel
        //public void CreateDoctorPanel()
        //{
        //    IEnumerable<Doctor> doctors;
        //    using (var context = new VeterinaryClinicDbContext())
        //    {
        //        doctors = context.Doctors.AsNoTracking().ToList();
        //    }

        //    #region input fields
        //    // Panel with work fields
        //    Panel fieldsPanel = CreateTextFieldsPanel();

        //    int locX = (int)(fieldsPanel.Width * 0.1),
        //        startLocY = (int)(fieldsPanel.Height * 0.1),
        //        tbWidth = (int)(fieldsPanel.Width * 0.8),
        //        stepLow = 25,
        //        stepFull;

        //    Label idLabel = new Label();
        //    idLabel.Location = new Point(10, 10);
        //    idLabel.Text = "Record №";
        //    idLabel.Width = 60;
        //    Label idValueLabel = new Label();
        //    idValueLabel.Location = new Point(70, 10);
        //    idValueLabel.Text = "#";

        //    Label nameLabel = new Label();
        //    nameLabel.Width = tbWidth;
        //    nameLabel.Text = "Enter first, second names:";
        //    nameLabel.Location = new Point(locX, startLocY);
        //    TextBox nameTb = new TextBox();
        //    nameTb.Location = new Point(locX, nameLabel.Location.Y + stepLow);
        //    nameTb.Width = tbWidth;
        //    nameTb.MaxLength = 120;

        //    // Calculate height of label+textBox
        //    stepFull = nameTb.Location.Y + nameTb.Height - nameLabel.Location.Y + 10;

        //    Label professionLabel = new Label();
        //    professionLabel.Location = new Point(locX, startLocY + stepFull);
        //    professionLabel.Text = "Enter profession:";
        //    TextBox professionTb = new TextBox();
        //    professionTb.Location = new Point(locX, professionLabel.Location.Y + stepLow);
        //    professionTb.Width = tbWidth;
        //    professionTb.MaxLength = 40;

        //    Label phoneLabel = new Label();
        //    phoneLabel.Location = new Point(locX, startLocY + stepFull * 2);
        //    phoneLabel.Text = "Enter phone:";
        //    MaskedTextBox phoneTb = new MaskedTextBox();
        //    phoneTb.Location = new Point(locX, phoneLabel.Location.Y + stepLow);
        //    phoneTb.Width = tbWidth;
        //    phoneTb.Mask = "(+38)\\000-000-00-00";

        //    Label emailLabel = new Label();
        //    emailLabel.Location = new Point(locX, startLocY + stepFull * 3);
        //    emailLabel.Text = "Enter email:";
        //    TextBox emailTb = new TextBox();
        //    emailTb.Location = new Point(locX, emailLabel.Location.Y + stepLow);
        //    emailTb.Width = tbWidth;
        //    #endregion

        //    Label statusLabel = new Label();
        //    statusLabel.Location = new Point(locX, startLocY + stepFull * 4);
        //    statusLabel.Text = statusPrefix;
        //    statusLabel.Width = tbWidth;

        //    // Panel with all doctor info
        //    Panel allInfo = CreateDataGridPanel();
        //    DataGridView doctorsGrid = CreateDataGrid(allInfo.Width, allInfo.Height);
        //    List<Control> searchControls = AddSearchControls(doctorsGrid, 0, "Search by name:");

        //    Button submitButton = new Button();
        //    submitButton.Name = "btnSubmit";
        //    submitButton.Location = new Point(locX, fieldsPanel.Height - 50);
        //    submitButton.Width = 60;
        //    submitButton.Text = "Submit";

        //    Button resetButton = new Button();
        //    resetButton.Name = "btnReset";
        //    resetButton.Location = new Point(locX + 80, fieldsPanel.Height - 50);
        //    resetButton.Width = 60;
        //    resetButton.Text = "Reset";

        //    resetButton.Click += (object sender, EventArgs e) =>
        //    {
        //        nameTb.Text = "";
        //        professionTb.Text = "";
        //        emailTb.Text = "";
        //        phoneTb.Text = "";
        //        idValueLabel.Text = "#";
        //    };

        //    submitButton.Click += (object sender, EventArgs e) =>
        //    {
        //        // Simple data validation
        //        if (!phoneTb.MaskCompleted || !emailTb.Text.Contains('@') || nameTb.Text.Length < 10)
        //        {
        //            ChangeStatusLabel(statusLabel, "Some data are not correct.", Color.Red);
        //            return;
        //        }

        //        using (var context = new VeterinaryClinicDbContext())
        //        {
        //            // Check for existing id on panel with data fields
        //            // # - not exists(new record) -> create new doctor and add to table
        //            // <number> - exists(old record) -> modify doctors data
        //            if (idValueLabel.Text == "#")
        //            {
        //                // Create new doctor and insert into context
        //                Doctor newDoc = new Doctor()
        //                {
        //                    PersonName = nameTb.Text,
        //                    Email = emailTb.Text,
        //                    Phone = phoneTb.Text,
        //                    Proffesion = professionTb.Text
        //                };

        //                try
        //                {
        //                    context.Doctors.Add(newDoc);
        //                    context.SaveChanges();
        //                    doctorsGrid.Rows.Add(newDoc.PersonName, newDoc.Proffesion, newDoc.Phone, newDoc.Email, newDoc.Id, "R", "M");
        //                    resetButton.PerformClick();

        //                    ChangeStatusLabel(statusLabel, "Doctor was added.", Color.Green);
        //                }
        //                catch
        //                {
        //                    ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
        //                }
        //            }
        //            else
        //            {
        //                // Modify doctor`s data
        //                try { 
        //                    Doctor doc = context.Doctors.First(d => d.Id == Convert.ToInt32(idValueLabel.Text));
        //                    doc.PersonName = nameTb.Text;
        //                    doc.Proffesion = professionTb.Text;
        //                    doc.Email = emailTb.Text;
        //                    doc.Phone = phoneTb.Text;
        //                    context.Doctors.Update(doc);
        //                    context.SaveChanges();

        //                    int rowIndexToUpdate = -1;
        //                    foreach (DataGridViewRow row in doctorsGrid.Rows)
        //                    {
        //                        if (row.Cells[4].Value.ToString().Equals(doc.Id.ToString()))
        //                        {
        //                            rowIndexToUpdate = row.Index;
        //                            break;
        //                        }
        //                    }

        //                    // Update data in grid row 
        //                    doctorsGrid.Rows[rowIndexToUpdate].Cells[0].Value = doc.PersonName;
        //                    doctorsGrid.CurrentRow.Cells[1].Value = doc.Proffesion;
        //                    doctorsGrid.CurrentRow.Cells[2].Value = doc.Phone;
        //                    doctorsGrid.CurrentRow.Cells[3].Value = doc.Email;

        //                    ChangeStatusLabel(statusLabel, "Doctor`s data is modified.", Color.Green);
        //                }
        //                catch
        //                {
        //                    ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
        //                }
        //            }
        //        }
        //    };

        //    if (doctors.Count() > 0)
        //    {
        //        doctorsGrid.Columns.Add("name", "Doctors Name:");
        //        doctorsGrid.Columns.Add("profession", "Profession:");
        //        doctorsGrid.Columns.Add("phone", "Phone:");
        //        doctorsGrid.Columns.Add("email", "Email:");
        //        doctorsGrid.Columns.Add("Id", "Id:");
        //        doctorsGrid.Columns[4].Visible = false;
        //        doctorsGrid.Columns.Add("remove", "");
        //        doctorsGrid.Columns.Add("modify", "");

        //        doctorsGrid.Columns[5].CellTemplate.Style.ForeColor = Color.Red;
        //        doctorsGrid.Columns[6].CellTemplate.Style.ForeColor = Color.Blue;

        //        doctorsGrid.CellClick += (object sender, DataGridViewCellEventArgs e) =>
        //        {
        //            // Empty Cells or cells not in columns 5 or 6 READONLY
        //            if ((doctorsGrid.CurrentCell.ColumnIndex == 5 || doctorsGrid.CurrentCell.ColumnIndex == 6) &&
        //                doctorsGrid.CurrentCell.Value == null)
        //                return;

        //            // Column [5] - REMOVE button
        //            if (doctorsGrid.CurrentCell.ColumnIndex == 5)
        //            {
        //                DialogResult dialogResult = MessageBox.Show("Remove doctor from list?", "Update data", MessageBoxButtons.YesNo);
        //                if (dialogResult == DialogResult.Yes)
        //                {
        //                    try {
        //                        using (var context = new VeterinaryClinicDbContext())
        //                        {
        //                            int docId = (int)doctorsGrid.CurrentRow.Cells[4].Value;
        //                            context.Doctors.Remove(context.Doctors.First(d => d.Id == docId));
        //                            context.SaveChanges();
        //                            doctorsGrid.Rows.Remove(doctorsGrid.CurrentRow);

        //                            if (idValueLabel.Text == docId.ToString())
        //                            {
        //                                idValueLabel.Text = "#";
        //                            }

        //                            ChangeStatusLabel(statusLabel, "Doctor was removed.", Color.Green);
        //                        }
        //                    } catch
        //                    {
        //                        ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
        //                    }
        //                }
        //            }
        //            // Column [6] - MODIFY button
        //            else if (doctorsGrid.CurrentCell.ColumnIndex == 6)
        //            {
        //                DialogResult dialogResult = MessageBox.Show("Change doctors data?", "Modify data", MessageBoxButtons.YesNo);
        //                if (dialogResult == DialogResult.Yes)
        //                {
        //                    idValueLabel.Text = doctorsGrid.CurrentRow.Cells[4].Value.ToString();
        //                    nameTb.Text = doctorsGrid.CurrentRow.Cells[0].Value.ToString();
        //                    professionTb.Text = doctorsGrid.CurrentRow.Cells[1].Value.ToString();
        //                    phoneTb.Text = doctorsGrid.CurrentRow.Cells[2].Value.ToString();
        //                    emailTb.Text = doctorsGrid.CurrentRow.Cells[3].Value.ToString();
        //                }
        //            }
        //        };

        //        // Create rows with doctor info in datagrid
        //        foreach (Doctor d in doctors)
        //        {
        //            doctorsGrid.Rows.Add(d.PersonName, d.Proffesion, d.Phone, d.Email, d.Id, "R", "M");
        //            doctorsGrid.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //            doctorsGrid.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //        }
        //    }

        //    // Apply controls to their panels
        //    List<Control> fields = new List<Control>()
        //    {
        //        idLabel, idValueLabel, 
        //        nameLabel, nameTb,
        //        professionLabel, professionTb,
        //        phoneLabel, phoneTb,
        //        emailLabel, emailTb,
        //        submitButton, resetButton, statusLabel
        //    };

        //    List<Control> table = new List<Control>();
        //    table.Add(doctorsGrid);
        //    table.AddRange(searchControls);

        //    AppendControls(fieldsPanel, fields);
        //    AppendControls(allInfo, table);
        //    AppendControls(destinationPanel, new List<Control> () { fieldsPanel, allInfo});
        //}
        //#endregion

        //#region Client Panel
        //public void CreateClientPanel()
        //{
        //    IEnumerable<Client> clients;
        //    using (var context = new VeterinaryClinicDbContext())
        //    {
        //        clients = context.Clients.AsNoTracking().ToList();
        //    }

        //    #region input fields
        //    // Panel with work fields
        //    Panel fieldsPanel = CreateTextFieldsPanel();

        //    int locX = (int)(fieldsPanel.Width * 0.1),
        //        startLocY = (int)(fieldsPanel.Height * 0.1),
        //        tbWidth = (int)(fieldsPanel.Width * 0.8),
        //        stepLow = 25,
        //        stepFull;

        //    Label idLabel = new Label();
        //    idLabel.Location = new Point(10, 10);
        //    idLabel.Text = "Record №";
        //    idLabel.Width = 60;
        //    Label idValueLabel = new Label();
        //    idValueLabel.Location = new Point(70, 10);
        //    idValueLabel.Text = "#";

        //    Label nameLabel = new Label();
        //    nameLabel.Width = tbWidth;
        //    nameLabel.Text = "Enter first, second names:";
        //    nameLabel.Location = new Point(locX, startLocY);
        //    TextBox nameTb = new TextBox();
        //    nameTb.Location = new Point(locX, nameLabel.Location.Y + stepLow);
        //    nameTb.Width = tbWidth;
        //    nameTb.MaxLength = 120;

        //    // Calculate height of label+textBox
        //    stepFull = nameTb.Location.Y + nameTb.Height - nameLabel.Location.Y + 10;

        //    Label phoneLabel = new Label();
        //    phoneLabel.Location = new Point(locX, startLocY + stepFull);
        //    phoneLabel.Text = "Enter phone:";
        //    MaskedTextBox phoneTb = new MaskedTextBox();
        //    phoneTb.Location = new Point(locX, phoneLabel.Location.Y + stepLow);
        //    phoneTb.Width = tbWidth;
        //    phoneTb.Mask = "(+38)\\000-000-00-00";

        //    Label emailLabel = new Label();
        //    emailLabel.Location = new Point(locX, startLocY + stepFull * 2);
        //    emailLabel.Text = "Enter email:";
        //    TextBox emailTb = new TextBox();
        //    emailTb.Location = new Point(locX, emailLabel.Location.Y + stepLow);
        //    emailTb.Width = tbWidth;
        //    #endregion

        //    Label statusLabel = new Label();
        //    statusLabel.Location = new Point(locX, startLocY + stepFull * 3);
        //    statusLabel.Text = statusPrefix;
        //    statusLabel.Width = tbWidth;

        //    // Panel with all clients info
        //    Panel allInfo = CreateDataGridPanel();
        //    DataGridView clientsGrid = CreateDataGrid(allInfo.Width, allInfo.Height);
        //    List<Control> searchControls = AddSearchControls(clientsGrid, 0, "Search by name:");

        //    #region action field buttons
        //    Button submitButton = new Button();
        //    submitButton.Name = "btnSubmit";
        //    submitButton.Location = new Point(locX, fieldsPanel.Height - 50);
        //    submitButton.Width = 60;
        //    submitButton.Text = "Submit";

        //    Button resetButton = new Button();
        //    resetButton.Name = "btnReset";
        //    resetButton.Location = new Point(locX + 80, fieldsPanel.Height - 50);
        //    resetButton.Width = 60;
        //    resetButton.Text = "Reset";

        //    resetButton.Click += (object sender, EventArgs e) =>
        //    {
        //        nameTb.Text = "";
        //        emailTb.Text = "";
        //        phoneTb.Text = "";
        //        idValueLabel.Text = "#";
        //    };

        //    submitButton.Click += (object sender, EventArgs e) =>
        //    {
        //        // Simple data validation
        //        if (!phoneTb.MaskCompleted || !emailTb.Text.Contains('@') || nameTb.Text.Length < 8)
        //        {
        //            ChangeStatusLabel(statusLabel, "Some data are not correct.", Color.Red);
        //            return;
        //        }

        //        using (var context = new VeterinaryClinicDbContext())
        //        {
        //            // Check for existing id on panel with data fields
        //            // # - not exists(new record) -> create new client and add to table
        //            // <number> - exists(old record) -> modify clients data
        //            if (idValueLabel.Text == "#")
        //            {
        //                // Create new client and insert into context
        //                Client newClient = new Client()
        //                {
        //                    PersonName = nameTb.Text,
        //                    Email = emailTb.Text,
        //                    Phone = phoneTb.Text,
        //                };

        //                try
        //                {
        //                    context.Clients.Add(newClient);
        //                    context.SaveChanges();
        //                    clientsGrid.Rows.Add(newClient.PersonName, newClient.Phone, newClient.Email, newClient.Id, "R", "M");
        //                    resetButton.PerformClick();

        //                    ChangeStatusLabel(statusLabel, "Client was added.", Color.Green);
        //                }
        //                catch
        //                {
        //                    ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
        //                }
        //            }
        //            else
        //            {
        //                // Modify clients`s data
        //                try
        //                {
        //                    Client client = context.Clients.First(d => d.Id == Convert.ToInt32(idValueLabel.Text));
        //                    client.PersonName = nameTb.Text;
        //                    client.Email = emailTb.Text;
        //                    client.Phone = phoneTb.Text;
        //                    context.Clients.Update(client);
        //                    context.SaveChanges();

        //                    int rowIndexToUpdate = -1;
        //                    foreach (DataGridViewRow row in clientsGrid.Rows)
        //                    {
        //                        if (row.Cells[3].Value.ToString().Equals(client.Id.ToString()))
        //                        {
        //                            rowIndexToUpdate = row.Index;
        //                            break;
        //                        }
        //                    }

        //                    // Update data in grid row 
        //                    clientsGrid.Rows[rowIndexToUpdate].Cells[0].Value = client.PersonName;
        //                    clientsGrid.CurrentRow.Cells[1].Value = client.Phone;
        //                    clientsGrid.CurrentRow.Cells[2].Value = client.Email;

        //                    ChangeStatusLabel(statusLabel, "Clients`s data is modified.", Color.Green);
        //                }
        //                catch
        //                {
        //                    ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
        //                }
        //            }
        //        }
        //    };

        //    #endregion

        //    #region grid building

        //    if (clients.Count() > 0)
        //    {
        //        // Create columns in datagrid
        //        clientsGrid.Columns.Add("name", "Clients Name:");
        //        clientsGrid.Columns.Add("phone", "Phone:");
        //        clientsGrid.Columns.Add("email", "Email:");
        //        clientsGrid.Columns.Add("Id", "Id:");
        //        clientsGrid.Columns[3].Visible = false;
        //        clientsGrid.Columns.Add("remove", "");
        //        clientsGrid.Columns.Add("modify", "");

        //        clientsGrid.Columns[4].CellTemplate.Style.ForeColor = Color.Red;
        //        clientsGrid.Columns[5].CellTemplate.Style.ForeColor = Color.Blue;

        //        clientsGrid.CellClick += (object sender, DataGridViewCellEventArgs e) =>
        //        {
        //            // Empty Cells or cells not in columns 4 or 5 READONLY
        //            if ((clientsGrid.CurrentCell.ColumnIndex == 4 || clientsGrid.CurrentCell.ColumnIndex == 5) &&
        //                clientsGrid.CurrentCell.Value == null)
        //                return;

        //            // Column [4] - REMOVE button
        //            if (clientsGrid.CurrentCell.ColumnIndex == 4)
        //            {
        //                DialogResult dialogResult = MessageBox.Show("Remove doctor from list?", "Update data", MessageBoxButtons.YesNo);
        //                if (dialogResult == DialogResult.Yes)
        //                {
        //                    try
        //                    {
        //                        using (var context = new VeterinaryClinicDbContext())
        //                        {
        //                            int clId = (int)clientsGrid.CurrentRow.Cells[3].Value;
        //                            context.Clients.Remove(context.Clients.First(d => d.Id == clId));
        //                            context.SaveChanges();
        //                            clientsGrid.Rows.Remove(clientsGrid.CurrentRow);

        //                            if (idValueLabel.Text == clId.ToString())
        //                            {
        //                                idValueLabel.Text = "#";
        //                            }

        //                            ChangeStatusLabel(statusLabel, "Client was removed.", Color.Green);
        //                        }
        //                    }
        //                    catch
        //                    {
        //                        ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
        //                    }
        //                }
        //            }
        //            // Column [5] - MODIFY button
        //            else if (clientsGrid.CurrentCell.ColumnIndex == 5)
        //            {
        //                DialogResult dialogResult = MessageBox.Show("Change clients data?", "Modify data", MessageBoxButtons.YesNo);
        //                if (dialogResult == DialogResult.Yes)
        //                {
        //                    idValueLabel.Text = clientsGrid.CurrentRow.Cells[3].Value.ToString();
        //                    nameTb.Text = clientsGrid.CurrentRow.Cells[0].Value.ToString();
        //                    phoneTb.Text = clientsGrid.CurrentRow.Cells[1].Value.ToString();
        //                    emailTb.Text = clientsGrid.CurrentRow.Cells[2].Value.ToString();
        //                }
        //            }
        //        };

        //        // Create rows with client info in datagrid
        //        foreach (Client c in clients)
        //        {
        //            clientsGrid.Rows.Add(c.PersonName, c.Phone, c.Email, c.Id, "R", "M");
        //            clientsGrid.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //            clientsGrid.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //        }
        //    }

        //    #endregion

        //    // Append all new controls to destination panel
        //    List<Control> fields = new List<Control>() {
        //        idLabel, idValueLabel,
        //        nameLabel, nameTb,
        //        phoneLabel, phoneTb,
        //        emailLabel, emailTb,
        //        submitButton, resetButton,
        //        statusLabel
        //    };
        //    List<Control> gridPanel = new List<Control>() {
        //        clientsGrid
        //    };
        //    gridPanel.AddRange(searchControls);
        //    AppendControls(fieldsPanel, fields);
        //    AppendControls(allInfo, gridPanel);
        //    AppendControls(destinationPanel, new List<Control>() { fieldsPanel, allInfo });
        //}
        //#endregion

        //#region VetProcedure Panel
        //public void CreateVetProcedurePanel()
        //{
        //    IEnumerable<VetProcedure> vetProcedures;
        //    using (var context = new VeterinaryClinicDbContext())
        //    {
        //        vetProcedures = context.VetProcedures.AsNoTracking().ToList();
        //    }

        //    #region input fields
        //    // Panel with work fields
        //    Panel fieldsPanel = CreateTextFieldsPanel();

        //    int locX = (int)(fieldsPanel.Width * 0.1),
        //        startLocY = (int)(fieldsPanel.Height * 0.1),
        //        tbWidth = (int)(fieldsPanel.Width * 0.8),
        //        stepLow = 25,
        //        stepFull;

        //    Label idLabel = new Label();
        //    idLabel.Location = new Point(10, 10);
        //    idLabel.Text = "Record №";
        //    idLabel.Width = 60;
        //    Label idValueLabel = new Label();
        //    idValueLabel.Location = new Point(70, 10);
        //    idValueLabel.Text = "#";

        //    Label nameLabel = new Label();
        //    nameLabel.Width = tbWidth;
        //    nameLabel.Text = "Enter procedure name:";
        //    nameLabel.Location = new Point(locX, startLocY);
        //    TextBox nameTb = new TextBox();
        //    nameTb.Location = new Point(locX, nameLabel.Location.Y + stepLow);
        //    nameTb.Width = tbWidth;
        //    nameTb.MaxLength = 60;

        //    // Calculate height of label+textBox
        //    stepFull = nameTb.Location.Y + nameTb.Height - nameLabel.Location.Y + 10;

        //    Label durationLabel = new Label();
        //    durationLabel.Location = new Point(locX, startLocY + stepFull);
        //    durationLabel.Text = "Enter duration time(mins):";
        //    durationLabel.Width = tbWidth;
        //    TextBox durationTb = new TextBox();
        //    durationTb.Location = new Point(locX, durationLabel.Location.Y + stepLow);
        //    durationTb.Width = tbWidth;

        //    Label priceLabel = new Label();
        //    priceLabel.Location = new Point(locX, startLocY + stepFull * 2);
        //    priceLabel.Text = "Enter price ($):";
        //    TextBox priceTb = new TextBox();
        //    priceTb.Location = new Point(locX, priceLabel.Location.Y + stepLow);
        //    priceTb.Width = tbWidth;
        //    #endregion

        //    Label statusLabel = new Label();
        //    statusLabel.Location = new Point(locX, startLocY + stepFull * 3);
        //    statusLabel.Text = statusPrefix;
        //    statusLabel.Width = tbWidth;

        //    // Panel with all procedure info
        //    Panel allInfo = CreateDataGridPanel();
        //    DataGridView vetProceduresGrid = CreateDataGrid(allInfo.Width, allInfo.Height);
        //    List<Control> searchControls = AddSearchControls(vetProceduresGrid, 0, "Search by name:");

        //    #region action buttons
        //    Button submitButton = new Button();
        //    submitButton.Name = "btnSubmit";
        //    submitButton.Location = new Point(locX, fieldsPanel.Height - 50);
        //    submitButton.Width = 60;
        //    submitButton.Text = "Submit";

        //    Button resetButton = new Button();
        //    resetButton.Name = "btnReset";
        //    resetButton.Location = new Point(locX + 80, fieldsPanel.Height - 50);
        //    resetButton.Width = 60;
        //    resetButton.Text = "Reset";

        //    resetButton.Click += (object sender, EventArgs e) =>
        //    {
        //        nameTb.Text = "";
        //        durationTb.Text = "";
        //        priceTb.Text = "";
        //        idValueLabel.Text = "#";
        //    };

        //    submitButton.Click += (object sender, EventArgs e) =>
        //    {
        //        // Simple data validation
        //        decimal durationTime = 0;
        //        decimal price = 0;

        //        if (nameTb.Text.Length < 5 || 
        //            !decimal.TryParse(durationTb.Text, out durationTime) ||
        //            !decimal.TryParse(priceTb.Text, out price)) 
        //        {
        //            ChangeStatusLabel(statusLabel, "Some data are not correct.", Color.Red);
        //            return;
        //        }

        //        using (var context = new VeterinaryClinicDbContext())
        //        {
        //            // Check for existing id on panel with data fields
        //            // # - not exists(new record) -> create new procedure and add to table
        //            // <number> - exists(old record) -> modify procedure data
        //            if (idValueLabel.Text == "#")
        //            {
        //                // Create new procedure and insert into context
        //                VetProcedure newProcedure = new VetProcedure()
        //                {
        //                    ProcedureName = nameTb.Text,
        //                    Duration = durationTime,
        //                    Price = price,
        //                };

        //                try
        //                {
        //                    context.VetProcedures.Add(newProcedure);
        //                    context.SaveChanges();
        //                    vetProceduresGrid.Rows.Add(newProcedure.ProcedureName, 
        //                        newProcedure.Duration.ToString("0.00"), 
        //                        newProcedure.Price.ToString("0.00"), 
        //                        newProcedure.Id, 
        //                        "R", "M");
        //                    resetButton.PerformClick();

        //                    ChangeStatusLabel(statusLabel, "Procedure was added.", Color.Green);
        //                }
        //                catch
        //                {
        //                    ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
        //                }
        //            }
        //            else
        //            {
        //                // Modify procedures`s data
        //                try
        //                {
        //                    VetProcedure vetProc = context.VetProcedures.First(vp => vp.Id == Convert.ToInt32(idValueLabel.Text));
        //                    vetProc.ProcedureName = nameTb.Text;
        //                    vetProc.Duration = durationTime;
        //                    vetProc.Price = price;
        //                    context.VetProcedures.Update(vetProc);
        //                    context.SaveChanges();

        //                    int rowIndexToUpdate = -1;
        //                    foreach (DataGridViewRow row in vetProceduresGrid.Rows)
        //                    {
        //                        if (row.Cells[3].Value.ToString().Equals(vetProc.Id.ToString()))
        //                        {
        //                            rowIndexToUpdate = row.Index;
        //                            break;
        //                        }
        //                    }

        //                    // Update data in grid row 
        //                    vetProceduresGrid.Rows[rowIndexToUpdate].Cells[0].Value = vetProc.ProcedureName;
        //                    vetProceduresGrid.CurrentRow.Cells[1].Value = vetProc.Duration.ToString("0.00");
        //                    vetProceduresGrid.CurrentRow.Cells[2].Value = vetProc.Price.ToString("0.00");

        //                    ChangeStatusLabel(statusLabel, "Clients`s data is modified.", Color.Green);
        //                }
        //                catch
        //                {
        //                    ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
        //                }
        //            }
        //        }
        //    };

        //    #endregion

        //    #region grid building

        //    if (vetProcedures.Count() > 0)
        //    {
        //        // Create columns in datagrid
        //        vetProceduresGrid.Columns.Add("name", "Procedures Name:");
        //        vetProceduresGrid.Columns.Add("duration", "Duration:");
        //        vetProceduresGrid.Columns.Add("price", "Price:");
        //        vetProceduresGrid.Columns.Add("Id", "Id:");
        //        vetProceduresGrid.Columns[3].Visible = false;
        //        vetProceduresGrid.Columns.Add("remove", "");
        //        vetProceduresGrid.Columns.Add("modify", "");

        //        vetProceduresGrid.Columns[4].CellTemplate.Style.ForeColor = Color.Red;
        //        vetProceduresGrid.Columns[5].CellTemplate.Style.ForeColor = Color.Blue;

        //        vetProceduresGrid.CellClick += (object sender, DataGridViewCellEventArgs e) =>
        //        {
        //            // Empty Cells or cells not in columns 4 or 5 READONLY
        //            if ((vetProceduresGrid.CurrentCell.ColumnIndex == 4 || vetProceduresGrid.CurrentCell.ColumnIndex == 5) &&
        //                vetProceduresGrid.CurrentCell.Value == null)
        //                return;

        //            // Column [4] - REMOVE button
        //            if (vetProceduresGrid.CurrentCell.ColumnIndex == 4)
        //            {
        //                DialogResult dialogResult = MessageBox.Show("Remove procedure from list?", "Update data", MessageBoxButtons.YesNo);
        //                if (dialogResult == DialogResult.Yes)
        //                {
        //                    try
        //                    {
        //                        using (var context = new VeterinaryClinicDbContext())
        //                        {
        //                            int clId = (int)vetProceduresGrid.CurrentRow.Cells[3].Value;
        //                            context.VetProcedures.Remove(context.VetProcedures.First(d => d.Id == clId));
        //                            context.SaveChanges();
        //                            vetProceduresGrid.Rows.Remove(vetProceduresGrid.CurrentRow);

        //                            if (idValueLabel.Text == clId.ToString())
        //                            {
        //                                idValueLabel.Text = "#";
        //                            }

        //                            ChangeStatusLabel(statusLabel, "Procedure was removed.", Color.Green);
        //                        }
        //                    }
        //                    catch
        //                    {
        //                        ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
        //                    }
        //                }
        //            }
        //            // Column [5] - MODIFY button
        //            else if (vetProceduresGrid.CurrentCell.ColumnIndex == 5)
        //            {
        //                DialogResult dialogResult = MessageBox.Show("Change procedure data?", "Modify data", MessageBoxButtons.YesNo);
        //                if (dialogResult == DialogResult.Yes)
        //                {
        //                    idValueLabel.Text = vetProceduresGrid.CurrentRow.Cells[3].Value.ToString();
        //                    nameTb.Text = vetProceduresGrid.CurrentRow.Cells[0].Value.ToString();
        //                    durationTb.Text = vetProceduresGrid.CurrentRow.Cells[1].Value.ToString();
        //                    priceTb.Text = vetProceduresGrid.CurrentRow.Cells[2].Value.ToString();
        //                }
        //            }
        //        };

        //        // Create rows with client info in datagrid
        //        foreach (VetProcedure vp in vetProcedures)
        //        {
        //            vetProceduresGrid.Rows.Add(vp.ProcedureName, vp.Duration, vp.Price, vp.Id, "R", "M");
        //            vetProceduresGrid.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //            vetProceduresGrid.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //        }
        //    }

        //    #endregion

        //    // Append all new controls to destination panel
        //    List<Control> fields = new List<Control>() {
        //        idLabel, idValueLabel,
        //        nameLabel, nameTb,
        //        durationLabel, durationTb,
        //        priceLabel, priceTb,
        //        submitButton, resetButton,
        //        statusLabel
        //    };

        //    List<Control> gridPanel = new List<Control>() {
        //        vetProceduresGrid
        //    };

        //    gridPanel.AddRange(searchControls);

        //    AppendControls(fieldsPanel, fields);
        //    AppendControls(allInfo, gridPanel);
        //    AppendControls(destinationPanel, new List<Control>() { fieldsPanel, allInfo });
        //}

        //#endregion

        //#region Pet Panel

        //public void CreatePetPanel()
        //{
        //    IEnumerable<Pet> pets;
        //    using (var context = new VeterinaryClinicDbContext())
        //    {
        //        pets = context.Pets.Include(pet => pet.Client).AsNoTracking().ToList();
        //    }

        //    #region input fields
        //    // Panel with work fields
        //    Panel fieldsPanel = CreateTextFieldsPanel();

        //    int locX = (int)(fieldsPanel.Width * 0.1),
        //        startLocY = (int)(fieldsPanel.Height * 0.1),
        //        tbWidth = (int)(fieldsPanel.Width * 0.8),
        //        stepLow = 25,
        //        stepFull;

        //    Label idLabel = new Label();
        //    idLabel.Location = new Point(10, 10);
        //    idLabel.Text = "Record №";
        //    idLabel.Width = 60;
        //    Label idValueLabel = new Label();
        //    idValueLabel.Location = new Point(70, 10);
        //    idValueLabel.Text = "#";

        //    Label nameLabel = new Label();
        //    nameLabel.Width = tbWidth;
        //    nameLabel.Text = "Enter pets name:";
        //    nameLabel.Location = new Point(locX, startLocY);
        //    TextBox nameTb = new TextBox();
        //    nameTb.Location = new Point(locX, nameLabel.Location.Y + stepLow);
        //    nameTb.Width = tbWidth;
        //    nameTb.MaxLength = 60;

        //    // Calculate height of label+textBox
        //    stepFull = nameTb.Location.Y + nameTb.Height - nameLabel.Location.Y + 10;

        //    Label typeLabel = new Label();
        //    typeLabel.Location = new Point(locX, startLocY + stepFull);
        //    typeLabel.Text = "Chose animal type:";
        //    typeLabel.Width = tbWidth;
        //    ComboBox typeCb = new ComboBox();
        //    typeCb.Location = new Point(locX, typeLabel.Location.Y + stepLow);
        //    typeCb.Width = tbWidth;
        //    typeCb.DropDownStyle = ComboBoxStyle.DropDownList;

        //    // Add animal type enum to combobox
        //    foreach (AnimalTypes nav in Enum.GetValues(typeof(AnimalTypes)))
        //    {
        //        typeCb.Items.Add(nav);

        //        if (typeCb.Items.Count > 0)
        //        {
        //            typeCb.SelectedIndex = 0;
        //        }
        //    }

        //    Label familyLabel = new Label();
        //    familyLabel.Location = new Point(locX, startLocY + stepFull * 2);
        //    familyLabel.Text = "Enter pets family:";
        //    familyLabel.Width = tbWidth;
        //    TextBox familyTb = new TextBox();
        //    familyTb.Location = new Point(locX, familyLabel.Location.Y + stepLow);
        //    familyTb.Width = tbWidth;


        //    Label weightLabel = new Label();
        //    weightLabel.Location = new Point(locX, startLocY + stepFull * 3);
        //    weightLabel.Text = "Enter pets Weight:";
        //    weightLabel.Width = tbWidth;
        //    TextBox weightTb = new TextBox();
        //    weightTb.Location = new Point(locX, weightLabel.Location.Y + stepLow);
        //    weightTb.Width = tbWidth;

        //    Label masterLabel = new Label();
        //    masterLabel.Location = new Point(locX, startLocY + stepFull * 4);
        //    masterLabel.Text = "Chose master:";
        //    masterLabel.Width = tbWidth;
        //    ComboBox masterCb = new ComboBox();
        //    masterCb.Location = new Point(locX, masterLabel.Location.Y + stepLow);
        //    masterCb.Width = tbWidth;
        //    masterCb.DropDownStyle = ComboBoxStyle.DropDownList;

        //    // Add all clients to combobox
        //    using (var context = new VeterinaryClinicDbContext())
        //    {
        //        foreach (var client in context.Clients.Select(c => new { c.Id, c.PersonName }).OrderBy(c => c.PersonName).ToList())
        //        {
        //            masterCb.Items.Add(new PetMasterComboItem() { Id = client.Id, ClientName = client.PersonName });
        //        }

        //        if (masterCb.Items.Count > 0)
        //        {
        //            masterCb.SelectedIndex = 0;
        //        }
        //    }
        //    #endregion

        //    Label statusLabel = new Label();
        //    statusLabel.Location = new Point(locX, startLocY + stepFull * 5);
        //    statusLabel.Text = statusPrefix;
        //    statusLabel.Width = tbWidth;

        //    // Panel with all pet info
        //    Panel allInfo = CreateDataGridPanel();
        //    DataGridView petsGrid = CreateDataGrid(allInfo.Width, allInfo.Height);
        //    List<Control> searchControls = AddSearchControls(petsGrid, 0, "Search by name:");

        //    #region action buttons
        //    Button submitButton = new Button();
        //    submitButton.Name = "btnSubmit";
        //    submitButton.Location = new Point(locX, fieldsPanel.Height - 50);
        //    submitButton.Width = 60;
        //    submitButton.Text = "Submit";

        //    Button resetButton = new Button();
        //    resetButton.Name = "btnReset";
        //    resetButton.Location = new Point(locX + 80, fieldsPanel.Height - 50);
        //    resetButton.Width = 60;
        //    resetButton.Text = "Reset";

        //    resetButton.Click += (object sender, EventArgs e) =>
        //    {
        //        nameTb.Text = "";
        //        weightTb.Text = "";
        //        familyTb.Text = "";
        //        idValueLabel.Text = "#";

        //        if (typeCb.Items.Count > 0)
        //        {
        //            typeCb.SelectedIndex = 0;
        //        }

        //        if (masterCb.Items.Count > 0)
        //        {
        //            masterCb.SelectedIndex = 0;
        //        }
        //    };

        //    submitButton.Click += (object sender, EventArgs e) =>
        //    {
        //        // Simple data validation
        //        double weight = 0;

        //        if (nameTb.Text.Length < 2 || 
        //            !double.TryParse(weightTb.Text, out weight) || 
        //            masterCb.SelectedItem == null || 
        //            typeCb.SelectedItem == null
        //            )
        //        {
        //            ChangeStatusLabel(statusLabel, "Some data are not correct.", Color.Red);
        //            return;
        //        }

        //        using (var context = new VeterinaryClinicDbContext())
        //        {
        //            // Check for existing id on panel with data fields
        //            // # - not exists(new record) -> create new procedure and add to table
        //            // <number> - exists(old record) -> modify procedure data
        //            if (idValueLabel.Text == "#")
        //            {
        //                // Create new procedure and insert into context
        //                Pet newPet = new Pet()
        //                {
        //                    PetName = nameTb.Text,
        //                    PetType = typeCb.SelectedItem.ToString(),
        //                    PetFamily = familyTb.Text,
        //                    Weight = weight,
        //                    ClientId = ((PetMasterComboItem)masterCb.SelectedItem).Id
        //                };

        //                try
        //                {
        //                    context.Pets.Add(newPet);
        //                    context.SaveChanges();
        //                    petsGrid.Rows.Add(
        //                        newPet.PetName,
        //                        ((PetMasterComboItem)masterCb.SelectedItem).ClientName,
        //                        newPet.PetType,
        //                        newPet.PetFamily,
        //                        newPet.Weight,
        //                        newPet.Id,
        //                        "R", "M");
        //                    resetButton.PerformClick();

        //                    ChangeStatusLabel(statusLabel, "Pet was added.", Color.Green);
        //                }
        //                catch
        //                {
        //                    ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
        //                }
        //            }
        //            else
        //            {
        //                // Modify procedures`s data
        //                try
        //                {
        //                    Pet pet = context.Pets.First(p => p.Id == Convert.ToInt32(idValueLabel.Text));
        //                    pet.PetName = nameTb.Text;
        //                    pet.PetType = typeCb.SelectedItem.ToString();
        //                    pet.Weight = weight;
        //                    pet.PetFamily = familyTb.Text;
        //                    pet.ClientId = ((PetMasterComboItem)masterCb.SelectedItem).Id;

        //                    context.Pets.Update(pet);
        //                    context.SaveChanges();

        //                    int rowIndexToUpdate = -1;
        //                    foreach (DataGridViewRow row in petsGrid.Rows)
        //                    {
        //                        if (row.Cells[5].Value.ToString().Equals(pet.Id.ToString()))
        //                        {
        //                            rowIndexToUpdate = row.Index;
        //                            break;
        //                        }
        //                    }

        //                    // Update data in grid row 
        //                    petsGrid.Rows[rowIndexToUpdate].Cells[0].Value = pet.PetName;
        //                    petsGrid.CurrentRow.Cells[1].Value = ((PetMasterComboItem)masterCb.SelectedItem).ClientName;
        //                    petsGrid.CurrentRow.Cells[2].Value = pet.PetType;
        //                    petsGrid.CurrentRow.Cells[3].Value = pet.PetFamily;
        //                    petsGrid.CurrentRow.Cells[4].Value = pet.Weight;

        //                    ChangeStatusLabel(statusLabel, "Pets`s data is modified.", Color.Green);
        //                }
        //                catch
        //                {
        //                    ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
        //                }
        //            }
        //        }
        //    };

        //    #endregion

        //    #region grid building

        //    if (pets.Count() > 0)
        //    {
        //        // Create columns in datagrid
        //        petsGrid.Columns.Add("name", "Pets Name:");
        //        petsGrid.Columns.Add("clName", "Master:");
        //        petsGrid.Columns.Add("type", "Type:");
        //        petsGrid.Columns.Add("family", "Family:");
        //        petsGrid.Columns.Add("weight", "Weight(kg):");
        //        petsGrid.Columns.Add("Id", "Id:");
        //        petsGrid.Columns[5].Visible = false;
        //        petsGrid.Columns.Add("remove", "");
        //        petsGrid.Columns.Add("modify", "");

        //        petsGrid.Columns[6].CellTemplate.Style.ForeColor = Color.Red;
        //        petsGrid.Columns[7].CellTemplate.Style.ForeColor = Color.Blue;

        //        petsGrid.CellClick += (object sender, DataGridViewCellEventArgs e) =>
        //        {
        //            // Empty Cells or cells not in columns 6 or 7 READONLY
        //            if ((petsGrid.CurrentCell.ColumnIndex == 6 || petsGrid.CurrentCell.ColumnIndex == 7) &&
        //                petsGrid.CurrentCell.Value == null)
        //                return;

        //            // Column [6] - REMOVE button
        //            if (petsGrid.CurrentCell.ColumnIndex == 6)
        //            {
        //                DialogResult dialogResult = MessageBox.Show("Remove pet from list?", "Update data", MessageBoxButtons.YesNo);
        //                if (dialogResult == DialogResult.Yes)
        //                {
        //                    try
        //                    {
        //                        using (var context = new VeterinaryClinicDbContext())
        //                        {
        //                            int petId = (int)petsGrid.CurrentRow.Cells[5].Value;
        //                            context.Pets.Remove(context.Pets.First(p => p.Id == petId));
        //                            context.SaveChanges();
        //                            petsGrid.Rows.Remove(petsGrid.CurrentRow);

        //                            if (idValueLabel.Text == petId.ToString())
        //                            {
        //                                idValueLabel.Text = "#";
        //                            }

        //                            ChangeStatusLabel(statusLabel, "Pet was removed.", Color.Green);
        //                        }
        //                    }
        //                    catch
        //                    {
        //                        ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
        //                    }
        //                }
        //            }
        //            // Column [7] - MODIFY button
        //            else if (petsGrid.CurrentCell.ColumnIndex == 7)
        //            {
        //                DialogResult dialogResult = MessageBox.Show("Change pet data?", "Modify data", MessageBoxButtons.YesNo);
        //                if (dialogResult == DialogResult.Yes)
        //                {
        //                    idValueLabel.Text = petsGrid.CurrentRow.Cells[5].Value.ToString();
        //                    nameTb.Text = petsGrid.CurrentRow.Cells[0].Value.ToString();
        //                    familyTb.Text = petsGrid.CurrentRow.Cells[3].Value.ToString();
        //                    weightTb.Text = petsGrid.CurrentRow.Cells[4].Value.ToString();

        //                    foreach (AnimalTypes a in typeCb.Items)
        //                    {
        //                        if (a.ToString() == petsGrid.CurrentRow.Cells[2].Value.ToString())
        //                        {
        //                            typeCb.SelectedItem = a;
        //                        }
        //                    }

        //                    foreach (PetMasterComboItem pm in masterCb.Items)
        //                    {
        //                        if (pm.ClientName == petsGrid.CurrentRow.Cells[1].Value.ToString())
        //                        {
        //                            masterCb.SelectedItem = pm;
        //                        }
        //                    }
        //                }
        //            }
        //        };

        //        // Create rows with client info in datagrid
        //        foreach (Pet p in pets)
        //        {
        //            petsGrid.Rows.Add(p.PetName, 
        //                p.Client.PersonName, 
        //                p.PetType,
        //                p.PetFamily,
        //                p.Weight,
        //                p.Id, 
        //                "R", "M");
        //            petsGrid.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //            petsGrid.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //            petsGrid.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //            petsGrid.Columns[7].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        //        }
        //    }

        //    #endregion

        //    // Append all new controls to destination panel
        //    List<Control> fields = new List<Control>() {
        //        idLabel, idValueLabel,
        //        nameLabel, nameTb,
        //        typeLabel, typeCb,
        //        familyLabel, familyTb,
        //        weightLabel, weightTb,
        //        masterLabel, masterCb,
        //        submitButton, resetButton,
        //        statusLabel
        //    };

        //    List<Control> gridPanel = new List<Control>() {
        //        petsGrid
        //    };

        //    gridPanel.AddRange(searchControls);

        //    AppendControls(fieldsPanel, fields);
        //    AppendControls(allInfo, gridPanel);
        //    AppendControls(destinationPanel, new List<Control>() { fieldsPanel, allInfo });
        //}
        //#endregion

        //#region helpers

        //private void ChangeStatusLabel(Label l, string text, Color textColor)
        //{
        //    l.Text = statusPrefix + text;
        //    l.ForeColor = textColor;
        //}

        //// Append to base Control (Panel) array of Control items(labels, textboxes... etc)
        //private void AppendControls(Control baseControl, List<Control> items)
        //{
        //    baseControl.Controls.Clear();

        //    foreach (Control item in items)
        //    {
        //        baseControl.Controls.Add(item);
        //    }
        //}
        //#endregion
    }
}
