﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VeterinaryClinic.Helpers
{
    abstract class PanelBase
    {
        protected FlowLayoutPanel destinationPanel;
        protected readonly string statusPrefix = "Status: ";

        public PanelBase(FlowLayoutPanel destinationPanel)
        {
            this.destinationPanel = destinationPanel;
        }

        // Append all separated components on single panel
        public abstract void BuildPanel();

        /*
         * Left side panel content
         * At info panels contains fields to input data
         */
        protected Panel CreateTextFieldsPanel()
        {
            Panel leftFieldsPanel = new Panel();
            leftFieldsPanel.Location = new Point(10, 10);
            leftFieldsPanel.Width = destinationPanel.Width / 3 - 20;
            leftFieldsPanel.Height = (int)(destinationPanel.Height * 0.85);
            leftFieldsPanel.BorderStyle = BorderStyle.FixedSingle;

            return leftFieldsPanel;
        }

        protected Panel CreateDataGridPanel()
        {
            Panel rightGridPanel = new Panel();
            rightGridPanel.Location = new Point((int)(destinationPanel.Width * 0.65), 10);
            rightGridPanel.Width = (int)(destinationPanel.Width * 0.65) - 20;
            rightGridPanel.Height = (int)(destinationPanel.Height * 0.85);
            //rightGridPanel.BackColor = Color.AliceBlue;

            return rightGridPanel;
        }

        protected DataGridView CreateDataGrid(int parentWidth, int parentHeight)
        {
            DataGridView grid = new DataGridView();
            grid.Width = (int)(parentWidth * 0.95);
            grid.Height = (int)(parentHeight * 0.85);
            grid.Location = new Point(10, 10);
            grid.ReadOnly = true;
            grid.AllowUserToResizeRows = false;
            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            grid.AllowUserToAddRows = false;

            return grid;
        }
        /*
         * dataGrid - current DataGridView
         * searchCellIndex - index of cell where we have to search
         * searchText - text for label, at left side of search textbox
         */
        protected List<Control> AddSearchControls(DataGridView dataGrid, int searchCellIndex, string searchText)
        {
            List<Control> searchControls = new List<Control>();

            // Search components
            Label searchLabel = new Label();
            searchLabel.Text = searchText;
            searchLabel.Location = new Point(10, dataGrid.Location.Y + dataGrid.Height + 30);
            searchLabel.Width = 100;
            searchControls.Add(searchLabel);
            TextBox searchTb = new TextBox();
            searchTb.Location = new Point(
                searchLabel.Location.X + searchLabel.Width + 10,
                searchLabel.Location.Y);
            searchTb.Width = 100;
            searchControls.Add(searchTb);
            Button searchButton = new Button();
            searchButton.Text = "Search";
            searchButton.Location = new Point(
                searchTb.Location.X + searchTb.Width + 10,
                searchTb.Location.Y);
            searchControls.Add(searchButton);
            Button searchResetButton = new Button();
            searchResetButton.Text = "Reset";
            searchResetButton.Location = new Point(
                searchButton.Location.X + searchButton.Width + 10,
                searchButton.Location.Y);
            searchControls.Add(searchResetButton);

            searchButton.Click += (object sender, EventArgs e) =>
            {
                if (dataGrid.Rows.Count > 0 && searchTb.Text.Trim() != "")
                {
                    foreach (DataGridViewRow row in dataGrid.Rows)
                    {
                        bool hasString = row.Cells[searchCellIndex].Value.ToString().ToLower().Contains(searchTb.Text.ToLower());
                        if (!hasString)
                        {
                            row.Visible = false;
                        }
                        else
                        {
                            row.Visible = true;
                        }
                    }
                }
                else if (searchTb.Text.Trim() == "")
                {
                    foreach (DataGridViewRow row in dataGrid.Rows)
                    {
                        row.Visible = true;
                    }
                }
            };

            searchResetButton.Click += (object sender, EventArgs e) =>
            {
                if (dataGrid.Rows.Count > 0)
                {
                    foreach (DataGridViewRow row in dataGrid.Rows)
                    {
                        row.Visible = true;
                    }
                }
            };

            return searchControls;
        }

        protected void ChangeStatusLabel(Label statusLabel, string text, Color textColor)
        {
            statusLabel.Text = statusPrefix + text;
            statusLabel.ForeColor = textColor;
        }

        // Append to base Control (Panel) array of Control items(labels, textboxes... etc)
        protected void AppendControls(Control baseControl, List<Control> items)
        {
            baseControl.Controls.Clear();

            foreach (Control item in items)
            {
                baseControl.Controls.Add(item);
            }
        }
    }
}
