﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VeterinaryClinic.Models;

namespace VeterinaryClinic.Helpers
{
    class PanelClients : PanelBase
    {
        public PanelClients(FlowLayoutPanel destinationPanel) : base(destinationPanel)
        {

        }

        public override void BuildPanel()
        {
            IEnumerable<Client> clients;
            using (var context = new VeterinaryClinicDbContext())
            {
                clients = context.Clients.AsNoTracking().ToList();
            }

            #region input fields
            // Panel with work fields
            Panel fieldsPanel = CreateTextFieldsPanel();

            int locX = (int)(fieldsPanel.Width * 0.1),
                startLocY = (int)(fieldsPanel.Height * 0.1),
                tbWidth = (int)(fieldsPanel.Width * 0.8),
                stepLow = 25,
                stepFull;

            Label idLabel = new Label();
            idLabel.Location = new Point(10, 10);
            idLabel.Text = "Record №";
            idLabel.Width = 60;
            Label idValueLabel = new Label();
            idValueLabel.Location = new Point(70, 10);
            idValueLabel.Text = "#";

            Label nameLabel = new Label();
            nameLabel.Width = tbWidth;
            nameLabel.Text = "Enter first, second names:";
            nameLabel.Location = new Point(locX, startLocY);
            TextBox nameTb = new TextBox();
            nameTb.Location = new Point(locX, nameLabel.Location.Y + stepLow);
            nameTb.Width = tbWidth;
            nameTb.MaxLength = 120;

            // Calculate height of label+textBox
            stepFull = nameTb.Location.Y + nameTb.Height - nameLabel.Location.Y + 10;

            Label phoneLabel = new Label();
            phoneLabel.Location = new Point(locX, startLocY + stepFull);
            phoneLabel.Text = "Enter phone:";
            MaskedTextBox phoneTb = new MaskedTextBox();
            phoneTb.Location = new Point(locX, phoneLabel.Location.Y + stepLow);
            phoneTb.Width = tbWidth;
            phoneTb.Mask = "(+38)\\000-000-00-00";

            Label emailLabel = new Label();
            emailLabel.Location = new Point(locX, startLocY + stepFull * 2);
            emailLabel.Text = "Enter email:";
            TextBox emailTb = new TextBox();
            emailTb.Location = new Point(locX, emailLabel.Location.Y + stepLow);
            emailTb.Width = tbWidth;
            #endregion

            Label statusLabel = new Label();
            statusLabel.Location = new Point(locX, startLocY + stepFull * 3);
            statusLabel.Text = statusPrefix;
            statusLabel.Width = tbWidth;

            // Panel with all clients info
            Panel allInfo = CreateDataGridPanel();
            DataGridView clientsGrid = CreateDataGrid(allInfo.Width, allInfo.Height);
            List<Control> searchControls = AddSearchControls(clientsGrid, 0, "Search by name:");

            #region action field buttons
            Button submitButton = new Button();
            submitButton.Name = "btnSubmit";
            submitButton.Location = new Point(locX, fieldsPanel.Height - 50);
            submitButton.Width = 60;
            submitButton.Text = "Submit";

            Button resetButton = new Button();
            resetButton.Name = "btnReset";
            resetButton.Location = new Point(locX + 80, fieldsPanel.Height - 50);
            resetButton.Width = 60;
            resetButton.Text = "Reset";

            resetButton.Click += (object sender, EventArgs e) =>
            {
                nameTb.Text = "";
                emailTb.Text = "";
                phoneTb.Text = "";
                idValueLabel.Text = "#";
            };

            submitButton.Click += (object sender, EventArgs e) =>
            {
                // Simple data validation
                if (!phoneTb.MaskCompleted || !emailTb.Text.Contains('@') || nameTb.Text.Length < 8)
                {
                    ChangeStatusLabel(statusLabel, "Some data are not correct.", Color.Red);
                    return;
                }

                using (var context = new VeterinaryClinicDbContext())
                {
                    // Check for existing id on panel with data fields
                    // # - not exists(new record) -> create new client and add to table
                    // <number> - exists(old record) -> modify clients data
                    if (idValueLabel.Text == "#")
                    {
                        // Create new client and insert into context
                        Client newClient = new Client()
                        {
                            PersonName = nameTb.Text,
                            Email = emailTb.Text,
                            Phone = phoneTb.Text,
                        };

                        try
                        {
                            context.Clients.Add(newClient);
                            context.SaveChanges();
                            clientsGrid.Rows.Add(newClient.PersonName, newClient.Phone, newClient.Email, newClient.Id, "R", "M");
                            resetButton.PerformClick();

                            ChangeStatusLabel(statusLabel, "Client was added.", Color.Green);
                        }
                        catch
                        {
                            ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
                        }
                    }
                    else
                    {
                        // Modify clients`s data
                        try
                        {
                            Client client = context.Clients.First(d => d.Id == Convert.ToInt32(idValueLabel.Text));
                            client.PersonName = nameTb.Text;
                            client.Email = emailTb.Text;
                            client.Phone = phoneTb.Text;
                            context.Clients.Update(client);
                            context.SaveChanges();

                            int rowIndexToUpdate = -1;
                            foreach (DataGridViewRow row in clientsGrid.Rows)
                            {
                                if (row.Cells[3].Value.ToString().Equals(client.Id.ToString()))
                                {
                                    rowIndexToUpdate = row.Index;
                                    break;
                                }
                            }

                            // Update data in grid row 
                            clientsGrid.Rows[rowIndexToUpdate].Cells[0].Value = client.PersonName;
                            clientsGrid.CurrentRow.Cells[1].Value = client.Phone;
                            clientsGrid.CurrentRow.Cells[2].Value = client.Email;

                            ChangeStatusLabel(statusLabel, "Clients`s data is modified.", Color.Green);
                        }
                        catch
                        {
                            ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
                        }
                    }
                }
            };

            #endregion

            #region grid building

            if (clients.Count() > 0)
            {
                // Create columns in datagrid
                clientsGrid.Columns.Add("name", "Clients Name:");
                clientsGrid.Columns.Add("phone", "Phone:");
                clientsGrid.Columns.Add("email", "Email:");
                clientsGrid.Columns.Add("Id", "Id:");
                clientsGrid.Columns[3].Visible = false;
                clientsGrid.Columns.Add("remove", "");
                clientsGrid.Columns.Add("modify", "");

                clientsGrid.Columns[4].CellTemplate.Style.ForeColor = Color.Red;
                clientsGrid.Columns[5].CellTemplate.Style.ForeColor = Color.Blue;

                clientsGrid.CellClick += (object sender, DataGridViewCellEventArgs e) =>
                {
                    // Empty Cells or cells not in columns 4 or 5 READONLY
                    if ((clientsGrid.CurrentCell.ColumnIndex == 4 || clientsGrid.CurrentCell.ColumnIndex == 5) &&
                        clientsGrid.CurrentCell.Value == null)
                        return;

                    // Column [4] - REMOVE button
                    if (clientsGrid.CurrentCell.ColumnIndex == 4)
                    {
                        DialogResult dialogResult = MessageBox.Show("Remove doctor from list?", "Update data", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            try
                            {
                                using (var context = new VeterinaryClinicDbContext())
                                {
                                    int clId = (int)clientsGrid.CurrentRow.Cells[3].Value;
                                    context.Clients.Remove(context.Clients.First(d => d.Id == clId));
                                    context.SaveChanges();
                                    clientsGrid.Rows.Remove(clientsGrid.CurrentRow);

                                    if (idValueLabel.Text == clId.ToString())
                                    {
                                        idValueLabel.Text = "#";
                                    }

                                    ChangeStatusLabel(statusLabel, "Client was removed.", Color.Green);
                                }
                            }
                            catch
                            {
                                ChangeStatusLabel(statusLabel, "Something went wrong.", Color.Red);
                            }
                        }
                    }
                    // Column [5] - MODIFY button
                    else if (clientsGrid.CurrentCell.ColumnIndex == 5)
                    {
                        DialogResult dialogResult = MessageBox.Show("Change clients data?", "Modify data", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            idValueLabel.Text = clientsGrid.CurrentRow.Cells[3].Value.ToString();
                            nameTb.Text = clientsGrid.CurrentRow.Cells[0].Value.ToString();
                            phoneTb.Text = clientsGrid.CurrentRow.Cells[1].Value.ToString();
                            emailTb.Text = clientsGrid.CurrentRow.Cells[2].Value.ToString();
                        }
                    }
                };

                // Create rows with client info in datagrid
                foreach (Client c in clients)
                {
                    clientsGrid.Rows.Add(c.PersonName, c.Phone, c.Email, c.Id, "R", "M");
                    clientsGrid.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    clientsGrid.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                }
            }

            #endregion

            // Append all new controls to destination panel
            List<Control> fields = new List<Control>() {
                idLabel, idValueLabel,
                nameLabel, nameTb,
                phoneLabel, phoneTb,
                emailLabel, emailTb,
                submitButton, resetButton,
                statusLabel
            };
            List<Control> gridPanel = new List<Control>() {
                clientsGrid
            };
            gridPanel.AddRange(searchControls);
            AppendControls(fieldsPanel, fields);
            AppendControls(allInfo, gridPanel);
            AppendControls(destinationPanel, new List<Control>() { fieldsPanel, allInfo });
        }
    }
}
