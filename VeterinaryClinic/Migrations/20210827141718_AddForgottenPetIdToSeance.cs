﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VeterinaryClinic.Migrations
{
    public partial class AddForgottenPetIdToSeance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PetId",
                table: "Seances",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Seances_PetId",
                table: "Seances",
                column: "PetId");

            migrationBuilder.AddForeignKey(
                name: "FK_Seances_Pets_PetId",
                table: "Seances",
                column: "PetId",
                principalTable: "Pets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Seances_Pets_PetId",
                table: "Seances");

            migrationBuilder.DropIndex(
                name: "IX_Seances_PetId",
                table: "Seances");

            migrationBuilder.DropColumn(
                name: "PetId",
                table: "Seances");
        }
    }
}
