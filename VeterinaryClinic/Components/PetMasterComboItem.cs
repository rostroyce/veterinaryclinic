﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeterinaryClinic.Components
{
    /*
     * Item for ComboBox on panel AddPet
     */
    class PetMasterComboItem
    {
        public int Id { get; set; }
        public string ClientName { get; set; }

        public override string ToString()
        {
            return ClientName;
        }
    }
}
