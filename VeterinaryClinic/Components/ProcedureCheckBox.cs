﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VeterinaryClinic.Components
{
    /*
     * Checkbox for SeanceWindow
     */
    public class ProcedureCheckBox : CheckBox
    {
        public int ProcedureId { get; set; }
        public decimal Duration { get; set; }
    }
}
