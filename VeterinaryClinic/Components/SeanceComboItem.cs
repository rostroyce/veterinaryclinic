﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeterinaryClinic.Components
{
    /*
     * Item for ComboBox`es in SeanceWindow
     */
    public class SeanceComboItem
    {
        public int EntityId { get; set; }
        public string EntityName { get; set; }

        public override string ToString()
        {
            return EntityName;
        }
    }
}
