﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VeterinaryClinic.Helpers;

namespace VeterinaryClinic.Components
{
    public class NavigationButton : Button
    {
        public Navigation Nav { get; set; }
    }
}
