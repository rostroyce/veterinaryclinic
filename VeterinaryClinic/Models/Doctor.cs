﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeterinaryClinic.Models
{
    public class Doctor : Person
    {
        [Required]
        [MaxLength(40)]
        public string Proffesion { get; set; }
    }
}
