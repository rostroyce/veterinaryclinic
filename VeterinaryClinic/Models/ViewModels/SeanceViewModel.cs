﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VeterinaryClinic.Helpers;

namespace VeterinaryClinic.Models.ViewModels
{
    /*
     * Model for panel Schedule
     * Represents some part of seance info
     */
    public class SeanceViewModel
    {
        public int SeanceId { get; set; }
        public string DoctorName { get; set; }
        public string ClientName { get; set; }
        public string PetType { get; set; }
        public string StartsIn { get; set; }
        public decimal Duration { get; set; }
    }
}
