﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VeterinaryClinic.Models
{
    public class Seance
    {
        public Seance()
        {
            this.VetProcedures = new HashSet<VetProcedure>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public int DoctorId { get; set; }
        public virtual Doctor Doctor { get; set; }
        public int PetId { get; set; }
        public virtual Pet Pet { get; set; }
        [Required]
        public DateTime SeanceDate { get; set; }
        public virtual ICollection<VetProcedure> VetProcedures { get; set; }
    }
}
