﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeterinaryClinic.Models
{
    public class VeterinaryClinicDbContext : DbContext
    {
        public VeterinaryClinicDbContext() : base()
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Pet> Pets { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Seance> Seances { get; set; }
        public DbSet<VetProcedure> VetProcedures { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLazyLoadingProxies()
                .UseSqlServer(@"Server=(LocalDb)\MSSQLLocalDB;Database=VeterinaryClinic;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(new User { Id=1, Username = "admin", Password = "admin" });

            //modelBuilder.Entity<Doctor>().HasData(new Doctor[] {
            //    new Doctor() { Id=1,
            //        PersonName="Sponge Bob Squire pants",
            //        Email="bikinobottom@gmail.com",
            //        Phone="321321321",
            //        Proffesion="Surgery" },
            //    new Doctor() { Id=2,
            //        PersonName="Patrick Star Zirko",
            //        Email="ThePatrick@gmail.com",
            //        Phone="132321312",
            //        Proffesion="Dantist" }
            //});
        }
    }
}

