﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VeterinaryClinic.Models
{
    public abstract class Person
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string PersonName { get; set; }
        [Required]
        [MaxLength(20)]
        public string Phone { get; set; }
        [MaxLength(80)]
        public string? Email { get; set; }
    }
}
