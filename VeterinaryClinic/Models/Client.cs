﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VeterinaryClinic.Models
{
    public class Client: Person
    {
        public virtual ICollection<Pet> Pets { get; set; }
    }
}
