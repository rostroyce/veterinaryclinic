﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VeterinaryClinic.Helpers;
using VeterinaryClinic.Models;
using VeterinaryClinic.Views.Login;

namespace VeterinaryClinic
{
    public partial class MainWindow : Form
    {
        readonly PanelBuilder panelBuilder;
        readonly SynchronizationContext UISyncContext;

        Navigation activePosition;

        public MainWindow()
        {
            InitializeComponent();
            InitializeDatabaseAsync();

            panelBuilder = new PanelBuilder(ContentPanel);
            UISyncContext = SynchronizationContext.Current;
        }

        private void AppClient_Load(object sender, EventArgs e)
        {
            this.Visible = false;
            DialogResult dialog = new LoginForm().ShowDialog();

            if (dialog == DialogResult.OK)
                this.Visible = true;
            else
                this.Dispose();

            btnSchedule.PerformClick();
        }

        private void btnAddDoctor_Click(object sender, EventArgs e)
        {
            NavButtonClickAsync(Navigation.Doctor);
            ChangeNavButtonColor((Button)sender);
        }

        private void btnAddClient_Click(object sender, EventArgs e)
        {
            NavButtonClickAsync(Navigation.Client);
            ChangeNavButtonColor((Button)sender);
        }

        private void btnAddProcedure_Click(object sender, EventArgs e)
        {
            NavButtonClickAsync(Navigation.VetProcedure);
            ChangeNavButtonColor((Button)sender);
        }

        private void btnPet_Click(object sender, EventArgs e)
        {
            NavButtonClickAsync(Navigation.Pet);
            ChangeNavButtonColor((Button)sender);
        }

        private void btnSchedule_Click(object sender, EventArgs e)
        {
            NavButtonClickAsync(Navigation.Schedule);
            ChangeNavButtonColor((Button)sender);
        }

        private async void NavButtonClickAsync(Navigation currentNav)
        {
            if (activePosition != currentNav)
            {
                ToggleButtons(false);
                activePosition = currentNav;
                Task buildPanel = Task.Run(() => 
                    CreateDataPanel(
                        () => 
                        { 
                            panelBuilder.Build(currentNav); 
                        })
                    );
                ShowLoadingTitle(buildPanel, "Loading.");
                await buildPanel;
                ToggleButtons(true);
            }
        }  

        private Task CreateDataPanel(Action createPanel)
        {
            return Task.Run(() => {
                UISyncContext.Send(
                    (t) =>
                    {
                        createPanel();
                    },
                    null);
            });
        }

        // Loading animation on form title while some task is acting
        private void ShowLoadingTitle(Task trackedTask, string text)
        {
            this.Text = text;
            Task showLoadingTask = Task.Run(() =>
            {
                while (!trackedTask.IsCompleted)
                {
                    Thread.Sleep(100);
                    if (this.Text.Length < 250)
                    {
                        UISyncContext.Send((t) =>
                        {
                            this.Text += ".";
                        },
                        null);
                    }
                }
            }).ContinueWith((t) => {
                UISyncContext.Send((t) =>
                {
                    this.Text = "Veterinary Clinic";

                },
                null);
            }, TaskContinuationOptions.OnlyOnRanToCompletion);
        }

        private async void InitializeDatabaseAsync()
        {
            ToggleButtons(false);
            Task t = Task.Run(() =>
            {
                using (var context = new VeterinaryClinicDbContext())
                {
                    context.Database.EnsureCreated();
                }
            });
            ShowLoadingTitle(t, "Initialize database.");
            await t;
            ToggleButtons(true);
            btnSchedule.PerformClick();
        }

        // Toggle navigation buttons.Enabled field
        // At async data downloading, all buttons need to block
        private void ToggleButtons(bool flag)
        {
            btnAddClient.Enabled = flag;
            btnAddDoctor.Enabled = flag;
            btnAddProcedure.Enabled = flag;
            btnSchedule.Enabled = flag;
            btnPet.Enabled = flag;
        }

        private void ChangeNavButtonColor(Button clickedButton)
        {
            foreach (Button b in NavigationPanel.Controls)
            {
                b.BackColor = Color.LightGray;   
            }

            clickedButton.BackColor = Color.Pink;
        }
    }
}